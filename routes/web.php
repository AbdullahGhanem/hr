<?php

use Illuminate\Support\Facades\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('locale/{locale}', 'HomeController@changeLang')->name('changeLang'); 

Auth::routes();
Route::post('login', 'Auth\LoginController@login')->name('login'); 

Route::group(['middleware' => ['localization'] ], function () {
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('showLoginForm'); 
});



Route::group(['middleware' => ['auth','localization'] ], function () {
    Route::group(['prefix' => 'Notification'], function() {
        Route::get('readAll', 'NotificationController@readAll')->name('notifications.read.all');
        Route::get('read/{id}', 'NotificationController@read')->name('notification.read');
    });

    Route::get('logout', 'Auth\LoginController@logout');
    Route::get('/', 'HomeController@index')->name('home');

    Route::group(['prefix' => 'supervisors'], function() {
        Route::get('{id}', 'EmployeeController@team')->name('supervisor.team');
    });

    Route::group(['prefix' => 'users'], function() {
        Route::get('', 'UserController@index')->name('users');
        Route::get('delete/{id}', 'UserController@destroy')->name('user.delete');

        Route::get('edit/{id}', 'UserController@edit')->name('user.edit');
        Route::post('update/{id}', 'UserController@update')->name('user.update');

        Route::group(['prefix' => 'employees'], function() {
            Route::get('', 'EmployeeController@index')->name('employees');
            Route::get('create', 'EmployeeController@create')->name('create.employee');
            Route::post('store', 'EmployeeController@store')->name('store.employee');
            Route::post('/store/skill', 'EmployeeController@storeSkill')->name('employee.store.skill');
            Route::get('{id}/password/change', 'EmployeeController@passwordChangeView')->name('employee.password.edit');
            Route::post('{id}/password/change', 'EmployeeController@passwordChange')->name('employee.password.update');
            Route::get('/{id}/create/skill', 'EmployeeController@createSkill')->name('employee.create.skill');
        });
        Route::group(['prefix' => 'admins'], function() {
            Route::get('/', 'AdminController@index')->name('admins');
        });
        Route::group(['prefix' => 'super-admins'], function() {
            Route::get('/', 'SuperAdminController@index')->name('super.admins');
        });
        Route::get('/{id}', 'EmployeeController@show')->name('employee');
    });

    Route::group(['prefix' => 'certificate'], function() {
        Route::get('create/{id}', 'CertificateController@create')->name('create.certificate');
        Route::post('store', 'CertificateController@store')->name('store.certificate');
        Route::get('delete/{id}', 'CertificateController@destroy')->name('certificate.delete');
        Route::get('edit/{id}', 'CertificateController@edit')->name('certificate.edit');
        Route::post('update/{id}', 'CertificateController@update')->name('certificate.update');
    });

    Route::group(['prefix' => 'recruitment'], function() {
        Route::get('create/{id}', 'RecruitmentController@create')->name('create.recruitment');
        Route::post('store', 'RecruitmentController@store')->name('store.recruitment');
        Route::get('edit/{id}', 'RecruitmentController@edit')->name('recruitment.edit');
        Route::post('update/{id}', 'RecruitmentController@update')->name('recruitment.update');
    });

    Route::group(['prefix' => 'experiance'], function() {
        Route::get('create/{id}', 'ExperianceController@create')->name('create.experiance');
        Route::post('store', 'ExperianceController@store')->name('store.experiance');
        Route::get('delete/{id}', 'ExperianceController@destroy')->name('experiance.delete');
        Route::get('edit/{id}', 'ExperianceController@edit')->name('experiance.edit');
        Route::post('update/{id}', 'ExperianceController@update')->name('experiance.update');
    });

    Route::group(['prefix' => 'familyMembers'], function() {
        Route::get('create/{id}', 'FamilyMembersController@create')->name('create.familyMembers');
        Route::post('store', 'FamilyMembersController@store')->name('store.familyMember');
        Route::get('delete/{id}', 'FamilyMembersController@destroy')->name('familyMember.delete');
        Route::get('edit/{id}', 'FamilyMembersController@edit')->name('familyMember.edit');
        Route::post('update/{id}', 'FamilyMembersController@update')->name('familyMember.update');
    });

    Route::group(['prefix' => 'socialStatus'], function() {
        Route::post('update/{id}', 'SocialStatusController@update')->name('update.socialStatus');
    });

    Route::group(['prefix' => 'departments'], function() {
        Route::get('', 'DepartmentController@index')->name('departments');
        Route::get('create', 'DepartmentController@create')->name('create.department');
        Route::post('store', 'DepartmentController@store')->name('store.department');
        Route::get('delete/{id}', 'DepartmentController@destroy')->name('department.delete');
        Route::get('edit/{id}', 'DepartmentController@edit')->name('department.edit');
        Route::post('update/{id}', 'DepartmentController@update')->name('department.update');
    });

    Route::group(['prefix' => 'jobTitles'], function() {
        Route::get('/', 'jobTitleController@index')->name('jobTitles');
        Route::get('create', 'jobTitleController@create')->name('create.jobTitle');
        Route::post('store', 'jobTitleController@store')->name('store.jobTitle');
        Route::get('delete/{id}', 'jobTitleController@destroy')->name('jobTitle.delete');
        Route::get('edit/{id}', 'jobTitleController@edit')->name('jobTitle.edit');
        Route::post('update/{id}', 'jobTitleController@update')->name('jobTitle.update');
    });
    
    Route::group(['prefix' => 'skills'], function() {
        Route::get('/', 'SkillController@index')->name('skills');
        Route::post('/store', 'SkillController@store')->name('skill.create');
    });

    Route::group(['prefix' => 'salaries'], function() {
        Route::get('/', 'SalaryController@index')->name('salaries');
        Route::get('delete/{id}', 'SalaryController@destroy')->name('salary.delete');
        Route::get('create/{employee_id}', 'SalaryController@create')->name('create.salary');
        Route::post('store', 'SalaryController@store')->name('store.salary');
        Route::get('edit/{id}', 'SalaryController@edit')->name('salary.edit');
        Route::post('update/{id}', 'SalaryController@update')->name('salary.update');
    });

    Route::group(['prefix' => 'contracts'], function() {
        Route::get('/', 'ContractController@index')->name('contracts');
        Route::get('delete/{id}', 'ContractController@destroy')->name('contract.delete');
        Route::get('create', 'ContractController@create')->name('create.contract');
        Route::post('store', 'ContractController@store')->name('store.contract');
        Route::get('edit/{id}', 'ContractController@edit')->name('contract.edit');
        Route::post('update/{id}', 'ContractController@update')->name('contract.update');
    });

});






