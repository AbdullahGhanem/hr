let modal = document.getElementById('add-skill-modal');
var btn = document.getElementById("add-skill");

var btn_close = document.getElementById("btn-close-skill-modal");
var close = document.getElementById("close-skill-modal");

// show modal
btn.onclick = function() {
	modal.classList.add('show');
    modal.style.display = "block";
}


btn_close.onclick = function() {
	modal.classList.remove('show');
    modal.style.display = "none";
}
close.onclick = function() {
	modal.classList.remove('show');
    modal.style.display = "none";
}

// on click outside hidde modal
document.onclick = function(event) {
    if (event.target == modal) {
		modal.classList.remove('show');
		modal.style.display = "none";
    }
}

