$(document).ready(function(){
	$('.btnDeleteContract').on('click', function (e) {
		e.preventDefault();
		var id = $(this).data("contract_id");
		renderDeleteModal(id , 'Contract' ,'/contracts/delete/');
    });
    
	$('.btnDeleteSalary').on('click', function (e) {
		e.preventDefault();
		var id = $(this).data("salary_id");
		renderDeleteModal(id , 'Salary' ,'/salaries/delete/');
    });
    
	$('.btnDeleteExperiance').on('click', function (e) {
		e.preventDefault();
		var id = $(this).data("experiances_id");
		renderDeleteModal(id , 'Experiance' ,'/experiance/delete/');
    });
    
	$('.btnDeleteCertificate').on('click', function (e) {
		e.preventDefault();
		var id = $(this).data("certificate_id");
		renderDeleteModal(id , 'Certificate' ,'/certificate/delete/');
    });
    
	$('.btnDeleteFamilyMember').on('click', function (e) {
        e.preventDefault();
		var id = $(this).data("familymember_id");
		renderDeleteModal(id , 'FamilyMember' ,'/familyMembers/delete/');
	});
});

function renderDeleteModal(id,type,route){
    console.log(id);
    document.getElementById('confirmationDelete').innerHTML = '';
    document.getElementById('type-id').innerHTML = '#'+id;
    document.getElementById('type').innerHTML = type;
    $('#delete-modal').modal('show');
    var confirmation = ``;
    confirmation += `
    <button type="button" 
            id="btn-close-delete-modal" 
            class="btn btn-secondary tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium"
            data-dismiss="modal">
        Close
    </button>
	<a href="`+route;
	confirmation += id;
	confirmation += `" class="btn btn-danger tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium"> Delete </a>`;
    document.getElementById('confirmationDelete').innerHTML = confirmation;
    
    $('#confirmationDelete').on('click', '#btn-close-delete-modal', function(event) {
        event.preventDefault();
        $('#delete-modal').modal('hide');
    });
}



