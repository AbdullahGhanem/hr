$(document).ready(function(){
	$('.btnDelete').on('click', function (e) {
		e.preventDefault();
		var id = $(this).data("salary_id");
		renderDeleteModal(id);
	});
});

function renderDeleteModal(id){
    console.log(id);
    document.getElementById('confirmationDelete').innerHTML = '';
    document.getElementById('type-id').innerHTML = '#'+id;
    document.getElementById('type').innerHTML = 'salary';
    $('#delete-modal').modal('show');
    var confirmation = ``;
    confirmation += `
    <button type="button" 
            id="btn-close-delete-modal" 
            class="btn btn-secondary tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium"
            data-dismiss="modal">
        Close
    </button>
	<a href="/salaries/delete/`;
	confirmation += id;
	confirmation += `" class="btn btn-danger tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium"> Delete Salary</a>`;
    document.getElementById('confirmationDelete').innerHTML = confirmation;
    
    $('#confirmationDelete').on('click', '#btn-close-delete-modal', function(event) {
        event.preventDefault();
        $('#delete-modal').modal('hide');
    });
}



