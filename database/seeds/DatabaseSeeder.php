<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->call(CountrySeeder::class);
        $this->call(CitySeeder::class);
        
        $this->call(DepartmentSeeder::class);
        $this->call(JobTitleSeeder::class);
        $this->call(UserTypeSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(ContractSeeder::class);
        $this->call(SkillSeeder::class);
        $this->call(SkillLevelSeeder::class);

        $this->call(UserSkillSeeder::class);
        $this->call(ExperienceSeeder::class);
        $this->call(RecruitmentSeeder::class);
        $this->call(CertificateSeeder::class);
        $this->call(SocialStatusSeeder::class);
        
        $this->call(FamilyMembersSeeder::class);
        $this->call(SalarySeeder::class);
    }
}
