<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;
use App\User;

class RecruitmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * recruitments
         */
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('recruitments')->truncate();
        $faker = Faker::create();
        $users = User::all();
        foreach ($users as $user ) {
            $service_availability = rand(0,1) ? true : false ;
            $completed = (bool)random_int(0, 1);
            DB::table('recruitments')->insert([
                'user_id' => $user->id,
                'completed' => ($service_availability) ? $completed : false,
                'start_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
                'end_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
                'service_availability' => $service_availability,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
