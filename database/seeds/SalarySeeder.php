<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;
use App\Contract;

class SalarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('salaries')->truncate();
        $faker = Faker::create();

        $contracts = Contract::where('is_active', 1 )->get();

        foreach ($contracts as $contract ) {
            $net_salary = $contract->monthly_salary;
            for ($i=0; $i < $faker->randomDigitNotNull ; $i++) { 
                if(rand(0,1)){
                    // take incentive ,allowance , other
                    $incentive = (rand(0,1)) ? $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 500.0) : 0.0;
                    $allowance = (rand(0,1)) ? $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 500.0) : 0.0;
                    $other = (rand(0,1)) ? $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 500.0) : 0.0;
                    $net_salary = $net_salary + ($allowance + $incentive + $other);
                    $deduction = 0.0;
                }else{
                    // take deduction
                    $deduction =(rand(0,1)) ? $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 500.0) : 0.0;
                    $net_salary = $net_salary - $deduction;
                    $incentive = 0.0;
                    $allowance = 0.0;
                    $other = 0.0;
                }

                //take loan
                $loan = (rand(0,1)) ? $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 500.0) : 0.0;
                $tax = $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 500.0);

                $total = $net_salary  - ($loan + $tax );

                $year = $monthly_salary = $faker->numberBetween($min = 10, $max = 18);

                
                DB::table('salaries')->insert([
                    'contract_id' => $contract->id,
                    'date' => $faker->date($format = '20'.$year.'-m-1', $max = 'now'),
                    'incentive' => $incentive,
                    'deduction' => $deduction,
                    'other' => $other,
                    'allowance' => $allowance,
                    'tax'=>$tax,
                    'net' => $net_salary,
                    'loan' => $loan,
                    'total' =>$total ,
                ]);
            }
        }
    }
}
