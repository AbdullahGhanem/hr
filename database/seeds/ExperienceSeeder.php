<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;
use App\User;

class ExperienceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('experiences')->truncate();
        $faker = Faker::create();
        $faker_ar = Faker::create('ar_SA');

        for ($i=0; $i < 4; $i++) { 
            DB::table('experiences')->insert([
                'user_id' => User::all()->random()->id,
                'start_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
                'end_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
                'role_en' => $faker->jobTitle,
                'role_ar' => $faker_ar->jobTitle,
                'description_en' => $faker->sentence($nbWords = 6, $variableNbWords = true),
                'description_ar' => 'لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور
                أنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا .',
                'where' => $faker->streetName,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
