<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class SkillLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('skill_levels')->truncate();

        $faker = Faker::create();
        $faker_ar = Faker::create('ar_SA');

        $levels = [
            'B1','B2','B3','B4', // Beginner
            'I1','I2','I3','I4', // Intermediate
            'E1','E2','E3','E4', // Expert
        ];

        foreach ($levels as $level ) {
            DB::table('skill_levels')->insert([
                'level_code' => $level,
                'description_en' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ratione, iure?',
                'description_ar' =>' لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
        
        
    }
}
