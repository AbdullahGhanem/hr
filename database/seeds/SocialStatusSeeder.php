<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\User;

class SocialStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('social_statuses')->truncate();
        
        $users = User::all();
        foreach ($users as $user ) {
            DB::table('social_statuses')->insert([
                'user_id' => $user->id,
                'is_bread_winner' => rand(0,1),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);        
        }
    }
}
