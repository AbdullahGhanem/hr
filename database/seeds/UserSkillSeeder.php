<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

use App\User;
use App\Skill;
use App\SkillLevel;
use App\UserSkill;

class UserSkillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('user_skills')->truncate();
        $faker = Faker::create();
        for ($i=0; $i < 2; $i++) { 
            $user_id = User::all()->random()->id;
            $skill_id = Skill::all()->random()->id;
            $skill_is_exist = UserSkill::where('user_id',$user_id)->where('skill_id',$skill_id)->first();
            if ($skill_is_exist) {
                $i--;
                continue;
            }else{
                $user_skill = new UserSkill;
                $user_skill->user_id = $user_id;
                $user_skill->skill_id = $skill_id;
                $user_skill->skill_level_id = SkillLevel::all()->random()->id;
                $user_skill->date_acquired = $faker->date($format = 'Y-m-d', $max = 'now');
                $user_skill->created_at = Carbon::now()->format('Y-m-d H:i:s');
                $user_skill->updated_at = Carbon::now()->format('Y-m-d H:i:s');
                $user_skill->save();
            }
        }
    }
}
