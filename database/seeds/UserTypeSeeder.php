<?php

use Illuminate\Database\Seeder;
use App\UserType;
use Faker\Factory as Faker;
use Carbon\Carbon;


class UserTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('user_types')->truncate();
        $faker = Faker::create();
        $types_en = ['employee','admin','superAdmin'];
        $types_ar = ['موظف','مشرف','مدير إدارى'];
        
        for ($i=0; $i < count($types_en); $i++) { 
            DB::table('user_types')->insert([
                'name_en' => $types_en[$i],
                'name_ar' => $types_ar[$i],
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
