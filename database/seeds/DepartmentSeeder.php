<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;
class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('departments')->truncate();
        $faker = Faker::create();
        $faker_ar = Faker::create('ar_SA');

        $departments_en = ['HR','Project Managment','Marketing','Accounting and Finance','R&D','Mobile Development','Web Development'];
        $departments_ar = ['الموارد البشرية','ادارة مشروع','تسويق','المحاسبة والتمويل','البحث والتطوير','تطوير المحمول','تطوير الشبكة'];
        for($i = 0 ; $i < count($departments_en) ; $i++) { 
            DB::table('departments')->insert([
                'name_en' => $departments_en[$i],
                'name_ar' => $departments_ar[$i],
                'description_en' => $faker->text($maxNbChars = 200),
                'description_ar' => $faker_ar->text($maxNbChars = 200),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
