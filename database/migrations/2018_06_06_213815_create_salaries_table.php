<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salaries', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('contract_id')->unsigned();
            $table->foreign('contract_id')->references('id')->on('contracts')->onDelete('cascade');

            $table->date('date');

            $table->decimal('incentive', 8, 2)->nullable();
            $table->decimal('deduction', 8, 2)->nullable();
            $table->decimal('other', 8, 2)->nullable();
            $table->decimal('allowance', 8, 2)->nullable();
            $table->decimal('tax', 8, 2)->nullable();
            $table->decimal('loan', 8, 2)->nullable();

            $table->decimal('net', 8, 2);

            $table->decimal('total', 8, 2); //variable salary from calculation
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salaries');
    }
}
