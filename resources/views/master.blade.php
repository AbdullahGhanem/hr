<!DOCTYPE html>
<html   lang="{{ app()->getLocale() }}" 
        dir="{{ (app()->getLocale() === 'ar') ? 'rtl' : '' }}" 
        class="{{ (app()->getLocale() === 'ar') ? 'rtl' : '' }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title> 
    @include('include.main-css')
    @yield('style')
</head>

<body>
    <div id="app">
        @include('include.top-nav-bar')
        @include('include.side-bar')
        <div class="br-mainpanel">
            @yield('sub-nav')

            @include('include.alert')
            {{-- @include('include.errors') --}}
            
            @yield('content') 
        </div>   
    </div>
    @include('include.main-js')
    @yield('script') 
</body>
</html>
