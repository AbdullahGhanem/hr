@extends('master') 
@section('title') All Skills
@endsection
 {{-- 
@section('sub-nav')
<div class="br-pageheader pd-y-15 pd-l-20">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item active" href="{{route('users')}}">Employees</a>
    </nav>
</div>
<!-- br-pageheader -->
@endsection
 --}} 
@section('content')
<div class="pd-x-30 pd-t-30">
    <h4 class="tx-gray-800 mg-b-5">{{ __('skills/skill.header')}}</h4>
    <p class="mg-b-0"></p>
</div>

<div class="br-pagebody">
    <div class="br-section-wrapper">
        <div class="br-pagebody pd-x-20 pd-sm-x-30">
            <h6 class="tx-inverse tx-uppercase tx-bold tx-14 ">{{ __('skills/skill.SKILL_TABLE')}}</h6>
            <p class="  mg-lg-b-50">{{ __('skills/skill.SUB_TITLE_SKILL_TABLE')}}</p>
            <button class=" mg-b-25 btn btn-danger pd-x-30 pd-y-15 tx-uppercase tx-bold {{(app()->getLocale() == 'ar') ? 'tx-spacing-1' : 'tx-spacing-6 ' }} tx-12" id="add-skill">
                    <i class="fa fa-plus {{(app()->getLocale() == 'ar') ? 'mg-l-10' : 'mg-r-10 ' }} "></i>
                    {{ __('skills/skill.CREAT_BUTTON')}}
                    </button>

            <div class="row">
                <div class="col-md-10">
                    <form action="{{route('skills')}}" method="get">
                        <div class="input-group">
                            <input type="text" class=" form-control" value="{{ request('skill')}}" name="skill" placeholder="{{ __('skills/skill.SEARCH_DEPT_NAME')}}">
                            <span class="input-group-btn">
                                <button  class="btn bd bg-white tx-gray-600" type="submit"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                        <!-- input-group -->
                    </form>
                </div>
                <div class="col-md-2">

                    <a href="{{route('skills')}}" class="btn btn-primary">{{ __('skills/skill.SEARCH_CLEAR_SEARCH_BTN')}}</a>
                </div>
            </div>

            <hr>
            <div class="skills">
                <ul>
                    @foreach ($skills as $skill )
                    <li class="lol">{{$skill->name}}</li>
                    @endforeach
                </ul>
            </div>

        </div>
    </div>
</div>
    @include('include.modals.add-skills')
@endsection
 
@section('script')
<script src="{{ asset('js/skill/modal.js') }}"></script>
@endsection