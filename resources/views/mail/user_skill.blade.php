@component('mail::message')
Hello {{$user->name_en}}

New skill added to you
.
@component('mail::table')
| Content       | Value         |
| ------------- |:-------------:|
| Name          | {{$skill->end_date}}       |
| Level         | {{$level->level_code}}     |
@endcomponent
@component('mail::button', ['url' => "{{route('employee',$user->id)}}"])
Your Account
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
