{{-- logo section --}}
{{-- logo section --}}
{{-- logo section --}}
{{-- logo section --}}
<div class="br-logo">
    <a href="">
        <span>[</span>{{__('navigation.logo')}}<span>]</span>
    </a>
</div>





<div class="br-header">

        {{-- left section --}}
        {{-- left section --}}
        {{-- left section --}}
        <div class="br-header-left">
          <div class="navicon-left hidden-md-down"><a id="btnLeftMenu" href=""><i class="icon ion-navicon-round"></i></a></div>
          <div class="navicon-left hidden-lg-up"><a id="btnLeftMenuMobile" href=""><i class="icon ion-navicon-round"></i></a></div>
        </div><!-- br-header-left -->



        {{-- right section --}}
        {{-- right section --}}
        {{-- right section --}}
        <div class="br-header-right">
          <nav class="nav">


          @if (app()->getLocale() == 'ar' )
              <a  href="/locale/en" class="btn-icon lang pd-l-30 pd-t-9 pd-r-30 tx-center">
                  <div><i class="fa fa-globe mg-l-10"></i>EN</div>
              </a>     
          @elseif(app()->getLocale() == 'en' )
              <a  href="/locale/ar" class="btn-icon lang pd-r-30 pd-t-9 pd-l-30 tx-center" >
                  <div><i class="fa fa-globe mg-r-10"></i>AR</div>
              </a>    
          @endif

            @include('include.notifications')
            
            <div class="dropdown">
              <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
                <span class="logged-name hidden-md-down">{{auth()->user()->name}}</span>
                @if (auth()->user()->image == null)
                  <img src="{{asset('img/6.jpg')}}" class="rounded-circle" alt="">
                @else
                  <img src="{{ asset('storage/images/users/'. auth()->user()->image) }}" class="rounded-circle" alt="">
                @endif
                <span class="square-10 bg-success"></span>
              </a>
              <div class="dropdown-menu dropdown-menu-header wd-200">
                <ul class="list-unstyled user-profile-nav">
                  <li><a href="{{route('employee',auth()->user()->id)}}"><i class="icon ion-ios-person"></i> {{__('navigation.viewprofile')}}</a></li>
                  <li><a href="{{route('user.edit',auth()->user()->id)}}"><i class="icon ion-ios-gear"></i> {{__('navigation.editprofile')}}</a></li>
                  <li>
                    <a href="{{ route('logout') }}">
                      <i class="icon ion-power"></i> {{__('navigation.logout')}}
                    </a>
                  </li>
                </ul>
              </div><!-- dropdown-menu -->
            </div><!-- dropdown -->
          </nav>
        </div><!-- br-header-right -->
      </div>