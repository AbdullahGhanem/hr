<?php

$users = (\Request::is('users/*') || \Request::is('users') || \Request::is('supervisors') || \Request::is('supervisors/*') ) ? "active" : "" ;
$departments = (\Request::is('departments') || \Request::is('departments/*')) ? "active" : "" ;
$jobTitles = (\Request::is('jobTitles') || \Request::is('jobTitles/*') ) ? "active" : "" ;
$salaries = (\Request::is('salaries') || \Request::is('salaries/*')) ? "active" : "" ;
$contracts = (\Request::is('contracts') || \Request::is('contracts/*')) ? "active" : "" ;
$skills = (\Request::is('skills')) ? "active" : "" ;
$dashboard =  (\Request::is('/')) ? "active" : ""  ;
?>

<div class="br-sideleft overflow-y-auto ps ps--theme_default ps--active-y" data-ps-id="c404b6ad-f42d-4aac-8458-640bfd5b2ce5">
        <label class="sidebar-label pd-x-15 mg-t-20 {{(app()->getLocale() == 'ar') ? 'tx-15' : 'tx-10' }}" >{{__('sidebar.nav')}}</label>
        <div class="br-sideleft-menu">
          <a href="{{route('home')}}" class="br-menu-link {{$dashboard}}">
            <div class="br-menu-item">
              <i class="menu-item-icon icon ion-ios-home-outline tx-24"></i>
              <span class="menu-item-label">{{__('sidebar.dashboard')}}</span>
            </div><!-- menu-item -->
          </a><!-- br-menu-link -->

          <a href="#" class="br-menu-link {{$users}}">
            <div class="br-menu-item">
              <i class="menu-item-icon fa fa-users tx-18"></i>
              <span class="menu-item-label">{{__('sidebar.users')}}</span>
              <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
          </a><!-- br-menu-link -->
          <ul class="br-menu-sub nav flex-column" >
            <li class="nav-item"><a href="{{route('users')}}" class="nav-link">{{__('sidebar.all')}}</a></li>
            <li class="nav-item"><a href="{{route('employees')}}" class="nav-link">{{__('sidebar.employees')}}</a></li>
            <li class="nav-item"><a href="{{route('admins')}}" class="nav-link">{{__('sidebar.admins')}}</a></li>
            <li class="nav-item"><a href="{{route('super.admins')}}" class="nav-link">{{__('sidebar.super_admins')}}</a></li>
            @if (auth()->user()->isSupervisor())
              <li class="nav-item"><a href="{{route('supervisor.team',auth()->user()->id)}}" class="nav-link">{{__('sidebar.team')}}</a></li>
            @endif
          </ul>

          <a href="{{route('departments')}}" class="br-menu-link {{$departments}}">
            <div class="br-menu-item">
              <i class="menu-item-icon fa fa-list tx-22"></i>
              <span class="menu-item-label">{{__('sidebar.departments')}}</span>
            </div><!-- menu-item -->
          </a><!-- br-menu-link -->


          <a href="{{route('jobTitles')}}" class="br-menu-link {{$jobTitles}}">
              <div class="br-menu-item">
                <i class="menu-item-icon icon ion-laptop tx-24"></i>
                <span class="menu-item-label">{{__('sidebar.job_titles')}}</span>
              </div><!-- menu-item -->
          </a><!-- br-menu-link -->

          <a href="{{route('salaries')}}" class="br-menu-link {{$salaries}}">
            <div class="br-menu-item">
              <i class="menu-item-icon fa fa-dollar tx-24"></i>
              <span class="menu-item-label">{{__('sidebar.salaries')}}</span>
            </div><!-- menu-item -->
          </a><!-- br-menu-link -->

          <a href="{{route('contracts')}}" class="br-menu-link {{$contracts}}">
              <div class="br-menu-item">
                <i class="menu-item-icon fa fa-check-square tx-22"></i>
                <span class="menu-item-label"> {{__('sidebar.contracts')}}</span>
              </div><!-- menu-item -->
          </a><!-- br-menu-link -->

          <a href="{{route('skills')}}" class="br-menu-link {{$skills}}">
              <div class="br-menu-item">
                <i class="menu-item-icon icon ion-ios-color-wand tx-24"></i>
                <span class="menu-item-label">{{__('sidebar.skills')}}</span>
              </div><!-- menu-item -->
          </a><!-- br-menu-link -->


        </div><!-- br-sideleft-menu -->
</div>