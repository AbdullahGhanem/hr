<div id="delete-modal" class="modal fade" >
    <div class="modal-dialog modal-dialog-vertical-center" role="document">
        <div class="modal-content bd-0 tx-14">
            <div class="modal-header pd-y-20 pd-x-25">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Delete</h6>
                <button type="button" id="close-delete-modal" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
           
                {{csrf_field()}}
                <div class="modal-body pd-25">
                    <p class="mg-b-5">
                        Are you sure you want to delete
                        <strong id="type" style="color:#dc3545"></strong>
                         ID :
                        <strong id="type-id" style="color:#dc3545">#1</strong>
                    </p>
                </div>
                <div class="modal-footer" id="confirmationDelete">                    
                </div>
        </div>
    </div>
</div>