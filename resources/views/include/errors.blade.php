@if ($errors->any())
<div class="alert alert-danger" style="height: auto;" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
    <div class="d-flex align-items-center justify-content-start">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li style="list-style: none;">
                            <i class="icon ion-ios-close alert-icon tx-24"></i>
                            <strong>Oh snap!</strong> {{ $error }}
                        </li>
                    @endforeach
                </ul>
    </div><!-- d-flex -->
</div><!-- alert -->
@endif