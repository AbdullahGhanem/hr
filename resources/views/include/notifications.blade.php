  <div class="dropdown">
        <a href="" class="nav-link pd-x-7 pos-relative" data-toggle="dropdown">
          <i class="icon ion-ios-bell-outline tx-24"></i>
          <!-- start: if statement -->
          @if (auth()->user()->unreadNotifications->count()) 
            <span class="square-8 bg-danger pos-absolute t-15 r-5 rounded-circle"></span>
          @endif
        
          <!-- end: if statement -->
        </a>
        <div class="dropdown-menu dropdown-menu-header wd-300 pd-0-force">
          <div class="d-flex align-items-center justify-content-between pd-y-10 pd-x-20 bd-b bd-gray-200">
            <label class="tx-12 tx-info tx-uppercase tx-semibold tx-spacing-2 mg-b-0">{{__('navigation.notification')}}</label>
            @if (auth()->user()->unreadNotifications->count()) 
            <a href="{{route('notifications.read.all')}}" class="tx-11">{{__('navigation.MarkRead')}}</a>
            @endif
          </div><!-- d-flex -->

          @if (auth()->user()->notifications->count()) 
          <div class="media-list notifications">
            <!-- loop starts here -->
            @foreach (auth()->user()->unreadNotifications as $notification)
              <a href="{{route('notification.read',$notification->id)}}" class="media-list-link read">
                <div class="media pd-x-20 pd-y-15">
                  @if ($notification->data['body']['type'] == "NewContract")
                    <i class="fa fa-file tx-30"></i>
                  @elseif($notification->data['body']['type'] == "NewSalary") 
                    <i class="fa fa-dollar tx-30"></i>
                  @elseif($notification->data['body']['type'] == "NewSkill")
                    <i class="fa fa-magic tx-30"></i>
                  @elseif($notification->data['body']['type'] == "NewEmployee")
                    <i class="fa fa-user tx-30"></i>
                  @endif
                  
                  <div class="media-body">
                    <p class="tx-13 mg-b-0 tx-gray-700"><strong class="tx-medium tx-gray-800"> {{$notification->data['body'][ Config::get('app.locale') ]['head']}}</strong>  {{$notification->data['body'][Config::get('app.locale')]['content']}}.</p>
                    <span class="tx-12">{{$notification->created_at->diffForHumans()}}</span>
                  </div>
                </div><!-- media -->
              </a>
            @endforeach

            @foreach (auth()->user()->readNotifications as $notification)
              <a href="{{route('notification.read',$notification->id)}}" class="media-list-link">
                <div class="media pd-x-20 pd-y-15">
                  @if ($notification->data['body']['type'] == "NewContract")
                    <i class="fa fa-file tx-30"></i>
                  @elseif($notification->data['body']['type'] == "NewSalary") 
                    <i class="fa fa-dollar tx-30"></i>
                  @elseif($notification->data['body']['type'] == "NewSkill")
                    <i class="fa fa-magic tx-30"></i>
                  @elseif($notification->data['body']['type'] == "NewEmployee")
                    <i class="fa fa-user tx-30"></i>
                  @endif
                  
                  <div class="media-body">
                    <p class="tx-13 mg-b-0 tx-gray-700"><strong class="tx-medium tx-gray-800"> {{$notification->data['body'][ Config::get('app.locale') ]['head']}}</strong>  {{$notification->data['body'][Config::get('app.locale')]['content']}}.</p>
                    <span class="tx-12">{{$notification->created_at->diffForHumans()}}</span>
                  </div>
                </div><!-- media -->
              </a>
            @endforeach
            
          </div><!-- media-list -->
          @else
          <center class="pd-t-10 pd-b-10">
            <h6>No notifications</h6>
          </center>
          @endif

          
        </div><!-- dropdown-menu -->
      </div><!-- dropdown -->