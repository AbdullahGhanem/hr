<div class="row no-gutters">
        {{-- Name EN --}}
        <div class="col-md-3">
            <div class="form-group ">
                <label class="form-control-label">
                    {{__('experience/create.FORM_EMPLOYEE_NAME')}}:
                    <span class="tx-danger">*</span>
                </label>
                <input disabled class="form-control is-invalid" value="{{$employee->name}}" type="text" name="name_en">
                <input   value="{{$employee->id}}" type="hidden" name="user_id">
            </div>
        </div>
    
    
        {{-- start_date  --}}
        <div class="col-md-3 mg-t--1 mg-md-t-0">
            <div class="form-group mg-md-l--1">
                <label class="form-control-label {{ ($errors->has('start_date')) ? ' is-invalid' : '' }}">
                    {{__('experience/create.FORM_STARTDATE')}}:
                    <span class="tx-danger">*</span>
                </label>
                <input required class="form-control" type="date" name="start_date" value="{{old('start_date')}}">
                <span class="form-error">
                        <small>{{ $errors->first('start_date') }}</small>
                </span>
            </div>
        </div>
    
    
        {{-- end_date --}}
        <div class="col-md-3 mg-t--1 mg-md-t-0">
            <div class="form-group mg-md-l--1">
                <label class="form-control-label {{ ($errors->has('end_date')) ? ' is-invalid' : '' }}">
                    {{__('experience/create.FORM_END_DATE')}}:
                    <span class="tx-danger">*</span>
                </label>
                <input required value="{{ old('end_date') }}" class="form-control" type="date" name="end_date">
                <span class="form-error">
                        <small>{{ $errors->first('end_date') }}</small>
                </span>
            </div>
        </div>

         {{-- Where --}}
         <div class="col-md-3 mg-t--1 mg-md-t-0">
                <div class="form-group mg-md-l--1">
                    <label class="form-control-label {{ ($errors->has('where')) ? ' is-invalid' : '' }}">
                        {{__('experience/create.FORM_WHERE')}}:
                        <span class="tx-danger">*</span>
                    </label>
                    <input required value="{{ old('where') }}" class="form-control" type="text" name="where" placeholder="{{__('experience/create.FORM_WHERE_PLACEHOLDER')}}">
                    <span class="form-error">
                            <small>{{ $errors->first('where') }}</small>
                    </span>
                </div>
            </div>






        {{--  Role In Arabic --}}
        <div class="col-md-6 ">
                <div class="form-group  bd-t-0-force">
                    <label class="form-control-label {{ ($errors->has('role_en')) ? ' is-invalid' : '' }}">
                            {{__('experience/create.FORM_ROLE_ARABIC')}}:
                        <span class="tx-danger">*</span>
                    </label>
                    <input required value="{{ old('role_en') }}" class="form-control" type="text" name="role_en" placeholder="{{__('experience/create.FORM_ROLE_AR_PLACEHOLDER')}}">
                    <span class="form-error">
                        <small>{{ $errors->first('role_en') }}</small>
                    </span>
                </div>
        </div>

         {{--  Role In English --}}
         <div class="col-md-6 mg-t--1 mg-md-t-0">
                <div class="form-group mg-md-l--1 bd-t-0-force">
                    <label class="form-control-label {{ ($errors->has('role_ar')) ? ' is-invalid' : '' }}">
                        {{__('experience/create.FORM_ROLE_ENGLISH')}}:
                        <span class="tx-danger">*</span>
                    </label>
                    <input value="{{ old('role_ar') }}" class="form-control" type="text" name="role_ar" placeholder=" {{__('experience/create.FORM_ROLE_EN_PLACEHOLDER')}}">
                    <span class="form-error">
                        <small>{{ $errors->first('role_ar') }}</small>
                    </span>
                </div>
        </div>


         {{--  Description In Arabic --}}
         <div class="col-md-6 ">
                <div class="form-group  bd-t-0-force">
                    <label class="form-control-label {{ ($errors->has('description_en')) ? ' is-invalid' : '' }}">
                        {{__('experience/create.FORM_DESCRIPTION_IN_ARABIC')}}:
                        <span class="tx-danger">*</span>
                    </label>
                    <textarea required cols="30" rows="10" name="description_en"  placeholder="{{__('experience/create.FORM_desc_AR_PLACEHOLDER')}}" class="form-control"></textarea>
                    <span class="form-error">
                        <small>{{ $errors->first('description_en') }}</small>
                    </span>
                </div>
        </div>

         {{--  Description In English --}}
         <div class="col-md-6 mg-t--1 mg-md-t-0">
                <div class="form-group mg-md-l--1 bd-t-0-force">
                    <label class="form-control-label {{ ($errors->has('description_ar')) ? ' is-invalid' : '' }}">
                        {{__('experience/create.FORM_DESCRIPTION_IN_ENGLISH')}}:
                        <span class="tx-danger">*</span>
                    </label>
                    <textarea required cols="30" rows="10" name="description_ar"  placeholder="{{__('experience/create.FORM_desc_EN_PLACEHOLDER')}}" class="form-control"></textarea>
                    <span class="form-error">
                        <small>{{ $errors->first('description_ar') }}</small>
                    </span>
                </div>
        </div>
    
    
    </div>
    <!-- row -->
    
    
    {{-- buttons --}}
    <div class="form-layout-footer bd pd-20 bd-t-0">
        <button class="btn btn-info">{{__('experience/create.CREAT_BUTTON')}}</button>
        <a href="{{route('employee',$employee->id.'?Experiance_page')}}" class="btn btn-secondary">{{__('experience/create.CREAT_CANCEL')}}</a>
    </div>
    <!-- form-group -->