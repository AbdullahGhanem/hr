<div class="col-lg-9">
        <div class="media-list bg-white rounded shadow-base tab-container">
                <div class=" pd-t-30 pd-r-20 pd-b-20 pd-l-20">
                        <h6 class="tx-inverse tx-uppercase tx-bold tx-14 ">{{ __('contracts/contract.CONTRACT_TABLE')}}</h6>
                        <p class="  mg-lg-b-10">
                            <small>{{$date}}</small>
                        </p>
                        <a href="{{route('create.contract')}}" class=" mg-b-25 btn btn-danger pd-x-30 pd-y-15 tx-uppercase tx-bold {{(app()->getLocale() == 'ar') ? 'tx-spacing-1' : 'tx-spacing-6 ' }} tx-12">
                            <i class="fa fa-plus {{(app()->getLocale() == 'ar') ? 'mg-l-10' : 'mg-r-10 ' }} "></i>
                            {{ __('contracts/contract.CREAT_BUTTON')}}
                        </a>

                        <div class="row">
                            <div class="col-md-9">
                                <form action="{{route('contracts')}}" method="get">
                                    <div class="input-group">
                                        <input type="text" class=" form-control" style="border-radius: 0px;" value="{{ request('emp_name')}}" name="emp_name" placeholder="{{ __('contracts/contract.SEARCH_FOR_NAME')}}...">
                                        <span class="input-group-btn">
                                            <button  class="btn bd bg-white tx-gray-600" style="border-radius: 0px;" type="submit"><i class="fa fa-search"></i></button>
                                        </span>
                                    </div>
                                    <!-- input-group -->
                                </form>
                            </div>
                            <div class="col-md-2">
                                <a href="{{route('contracts')}}" class="btn btn-primary">{{ __('contracts/contract.SEARCH_CLEAR_SEARCH_BTN')}}</a>
                            </div>
                        </div>
                        <hr>

        
                        <div class=" rounded table-responsive">
                            <table class="table table-striped table-bordered table-hover mg-b-0">
                                <thead class="thead-colored thead-dark">
                                    <tr>
                                        <td>#</td>
                                        <td>{{ __('contracts/contract.TABLE_HEAD_Employee')}}</td>
                                        <td>{{ __('contracts/contract.TABLE_HEAD_START_Date')}}</td>
                                        <td>{{ __('contracts/contract.TABLE_HEAD_END_Date')}}</td>
                                        <td>{{ __('contracts/contract.TABLE_HEAD_Active')}}</td>
                                        <td>{{ __('contracts/contract.TABLE_HEAD_OPTIONS')}}</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($contracts as $contract)
                                        <tr>
                                            <td>
                                                {{$contract->id }}
                                            </td>
                                            <td class="user-info-table">
                                                <a href="{{route('employee',$contract->user->id.'?Contract_page')}}">
                                                    @if ($contract->user->image == null)
                                                        <img src="{{asset('img/6.jpg')}}" class="user-image rounded-circle" alt="">
                                                    @else
                                                        <img src="{{ asset('storage/images/users/'. $contract->user->image) }}" class="user-image rounded-circle" alt="">
                                                    @endif
                                                    {{$contract->user->name}}
                                                </a>  
                                                <div class="user-info" >
                                                    <small><span class="label ">{{ __('contracts/contract.TABLE_JOBTITLE')}}</span>  : {{$contract->user->jobTitle->name}}</small> 
                                                </div>
                                                <div class="user-info" >
                                                    @if ($contract->user->type->name_en == 'employee')
                                                    <span class="square-8 rounded-circle bg-dark mg-r-10"></span>
                                                    @elseif($contract->user->type->name_en == 'admin')
                                                    <span class="square-8 rounded-circle bg-warning mg-r-10"></span>
                                                    @elseif($contract->user->type->name_en == 'superAdmin')
                                                    <span class="square-8 rounded-circle bg-danger mg-r-10"></span>
                                                    @endif
                                                    <small>{{$contract->user->type->name}} </small>
                                                </div> 
                                            </td>
        
                                            <td>
                                                    {{$contract->start_date }}
                                            </td>
                                            <td>
                                                    {{$contract->end_date }}
                                            </td>
                                            <td>
                                                <div class="display-flex">
                                                    @if ($contract->is_active)
                                                    <div class="circle circle-success"></div>
                                                    @else
                                                    <div class="circle circle-error"></div>
                                                    @endif
                                                </div>      
                                            </td>
                                            <td class="options">
                                                <a href="{{route('employee',$contract->user->id)}}">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                <a href="{{route('contract.edit',$contract->id)}}">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                <a href="" class="btnDelete" data-contract_id="{{$contract->id}}">
                                                    <i class="fa fa-trash"></i>               
                                                </a>                
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
        
                        {{ $contracts->appends($_GET)->links() }}
                </div> 
        </div><!-- card -->
    </div><!-- col-lg-8 -->   <!-- right part -->








            
