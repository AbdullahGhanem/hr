<div class="form-layout form-layout-1">
    <div class="row mg-b-25">

        <div class="col-lg-6">
            <div class="form-group">
                <label class="form-control-label {{ ($errors->has('name_en')) ? ' is-invalid' : '' }}">
                    {{ __('departments/create.FORM_NAME_EN')}}:
                    <span class="tx-danger">*</span>
                </label>
                <input class="form-control" type="text" name="name_en" placeholder=" {{ __('departments/create.FORM_NAME_EN_PLACEHOLDER')}}">
                <span class="form-error">
                    <small>{{ $errors->first('name_en') }}</small>
                </span>
            </div>
        </div>
        <!-- col-6 -->
        <div class="col-lg-6">
            <div class="form-group">
                <label class="form-control-label {{ ($errors->has('description_en')) ? ' is-invalid' : '' }}">
                    {{ __('departments/create.FORM_Description_EN')}}:
                    <span class="tx-danger">*</span>
                </label>
                <input class="form-control" type="text" name="description_en" placeholder=" {{ __('departments/create.FORM_DESC_EN_PLACEHOLDER')}}">
                <span class="form-error">
                    <small>{{ $errors->first('description_en') }}</small>
                </span>
            </div>
        </div>
        <!-- col-6 -->

        <div class="col-lg-6">
            <div class="form-group">
                <label class="form-control-label {{ ($errors->has('name_ar')) ? ' is-invalid' : '' }}">
                    {{ __('departments/create.FORM_NAME_AR')}}:
                    <span class="tx-danger">*</span>
                </label>
                <input class="form-control" type="text" name="name_ar" placeholder=" {{ __('departments/create.FORM_NAME_AR_PLACEHOLDER')}}">
                <span class="form-error">
                    <small>{{ $errors->first('name_ar') }}</small>
                </span>
            </div>
        </div>
        <!-- col-6 -->
        <div class="col-lg-6">
            <div class="form-group">
                <label class="form-control-label {{ ($errors->has('description_ar')) ? ' is-invalid' : '' }}">
                    {{ __('jobTitles/create.FORM_Description_AR')}}:
                    <span class="tx-danger">*</span>
                </label>
                <input class="form-control" type="text" name="description_ar" placeholder=" {{ __('departments/create.FORM_DESC_AR_PLACEHOLDER')}}">
                <span class="form-error">
                    <small>{{ $errors->first('description_ar') }}</small>
                </span>
            </div>
        </div>
        <!-- col-6 -->


    </div>
    <!-- row -->

    <div class="form-layout-footer">
        <button type="submit" class="btn btn-info">{{ __('jobTitles/create.CREAT_BUTTON')}}</button>
        <a href="{{route('departments')}}" class="btn btn-secondary">{{ __('jobTitles/create.CREAT_CANCEL')}}</a>
    </div>
    <!-- form-layout-footer -->
</div>