<div class="row no-gutters">
    {{-- Name EN --}}
    <div class="col-md-4">
        <div class="form-group ">
            <label class="form-control-label">
                    {{__('recruitment/create.FORM_EMPLOYEE_NAME')}}:
                    <span class="tx-danger">*</span>
                </label>
            <input disabled class="form-control is-invalid" value="{{$employee->name}}" type="text" name="name_en">
            <input value="{{$employee->id}}" type="hidden" name="user_id">
        </div>
    </div>


    {{-- start_date --}}
    <div class="col-md-4 mg-t--1 mg-md-t-0">
        <div class="form-group mg-md-l--1">
            <label class="form-control-label {{ ($errors->has('start_date')) ? ' is-invalid' : '' }}">
                    {{__('recruitment/create.FORM_START_DATE')}}:
                    <span class="tx-danger">*</span>
                </label>
            <input required class="form-control" type="date" name="start_date" value="{{old('start_date')}}">
            <span class="form-error">
                        <small>{{ $errors->first('start_date') }}</small>
                </span>
        </div>
    </div>


    {{-- end_date --}}
    <div class="col-md-4 mg-t--1 mg-md-t-0">
        <div class="form-group mg-md-l--1">
            <label class="form-control-label {{ ($errors->has('end_date')) ? ' is-invalid' : '' }}">
                    {{__('recruitment/create.FORM_END_DATE')}}:
                    <span class="tx-danger">*</span>
                </label>
            <input required class="form-control" type="date" name="end_date" value="{{old('end_date')}}">
            <span class="form-error">
                        <small>{{ $errors->first('end_date') }}</small>
                </span>
        </div>
    </div>



    {{-- service_availability --}}
    <div class="col-md-6 ">
        <div class="form-group  bd-t-0-force">
            <label class="form-control-label {{ ($errors->has('availability')) ? ' is-invalid' : '' }}">
                            {{__('recruitment/create.FORM_Service_Availability')}}:
                        <span class="tx-danger">*</span>
            </label>
            <label class="switch">
                                    <input type="checkbox" name="availability" >
                                    <span class="slider round"></span>
            </label>
            <span class="form-error">
                                    <small>{{ $errors->first('availability') }}</small>
            </span>
        </div>
    </div>

    {{-- completed --}}
    <div class="col-md-6 mg-t--1 mg-md-t-0">
        <div class="form-group mg-md-l--1 bd-t-0-force">

            <label class="form-control-label {{ ($errors->has('completed')) ? ' is-invalid' : '' }}">
                                {{__('recruitment/create.FORM_completed')}}:
                            <span class="tx-danger">*</span>
                            </label>
            <label class="switch">
                                        <input type="checkbox" name="completed" >
                                        <span class="slider round"></span>
                            </label>
            <span class="form-error">
                                        <small>{{ $errors->first('completed') }}</small>
                            </span>


        </div>
    </div>


</div>
<!-- row -->


{{-- buttons --}}
<div class="form-layout-footer bd pd-20 bd-t-0">
    <button type="submit" class="btn btn-info">{{__('recruitment/create.CREAT_BUTTON')}}</button>
    <a href="{{route('employee',$employee->id.'?Recruitment_page')}}" class="btn btn-secondary">{{__('recruitment/create.CREAT_CANCEL')}}</a>
</div>
<!-- form-group -->