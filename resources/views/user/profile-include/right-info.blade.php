<?php
	
  $Contracts = request()->has('Contract_page') ? "active" : ""; 
  $Salaries = request()->has('Salarie_page') ? "active" : ""; 
  $Experiances = request()->has('Experiance_page') ? "active" : ""; 
  $Certificates = request()->has('Certificate_page') ? "active" : ""; 
  $Recruitments = request()->has('Recruitment_page') ? "active" : ""; 
  $Family = request()->has('Family_page') ? "active" : ""; 
  if(count(request()->all()) == 0 ){
    $Contracts = "active";
	}
?>
<div class="col-lg-8">
        <div class="media-list bg-white rounded shadow-base tab-container">
          <div class="ht-md-60 pd-x-20 bg-dark rounded d-flex align-items-center justify-content-center links-container">
            <ul class="nav nav-outline nav-outline-for-dark align-items-center flex-column flex-md-row" role="tablist">
                          <li class="nav-item">
                            <a class="nav-link {{(app()->getLocale() == 'ar') ? 'tx-16' : 'tx-12' }} {{$Contracts}}"  href="?Contract_page" > {{ __('users/profile.title_contracts')}}</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link {{(app()->getLocale() == 'ar') ? 'tx-16' : 'tx-12' }} {{$Salaries}}"  href="?Salarie_page"> {{ __('users/profile.title_Salaries')}}</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link {{(app()->getLocale() == 'ar') ? 'tx-16' : 'tx-12' }} {{$Experiances}}"  href="?Experiance_page"> {{ __('users/profile.title_Experiances')}}</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link {{(app()->getLocale() == 'ar') ? 'tx-16' : 'tx-12' }} {{$Certificates}}"  href="?Certificate_page" > {{ __('users/profile.title_Certificates')}}</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link {{(app()->getLocale() == 'ar') ? 'tx-16' : 'tx-12' }} {{$Recruitments}}"  href="?Recruitment_page"> {{ __('users/profile.title_Recruitment')}}</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link {{(app()->getLocale() == 'ar') ? 'tx-16' : 'tx-12' }} {{$Family}}"  href="?Family_page"> {{ __('users/profile.title_Family_Members')}}</a>
                          </li>
            </ul>
          </div>

          <div class="media  pd-xs-30"> 
            <div class="tab-panel {{$Contracts}}">
              @include('user.profile-include.content.contracts')
            </div>
             <div class="tab-panel {{$Salaries}}">
                @include('user.profile-include.content.salaries')
            </div>
            <div class="tab-panel {{$Experiances}}">
               @include('user.profile-include.content.experiances')
            </div>
            <div class="tab-panel {{$Certificates}}">
               @include('user.profile-include.content.certificates')
            </div>
            <div class="tab-panel {{$Recruitments}}">
               @include('user.profile-include.content.recruitments')
            </div>
            <div class="tab-panel {{$Family}}">
               @include('user.profile-include.content.familyMembers')
            </div>
          </div><!-- media -->

        </div><!-- card -->
</div><!-- col-lg-8 -->