<div class="col-lg-4 mg-t-30 mg-lg-t-0">
        <div class="card pd-20 pd-xs-30 shadow-base bd-0">
            <h6 class="tx-gray-800 tx-uppercase tx-semibold {{(app()->getLocale() == 'ar') ? 'tx-20' : 'tx-13' }} mg-b-25">{{ __('users/profile.Form_Information')}} </h6>
            <div class="img" 
                style="background-image:url('{{asset('storage/images/users/'. $employee->image)}}');"
                >
            </div>
            @if (auth()->user()->id === $employee->id)
                <a href="{{route('employee.password.edit',$employee->id)}}" class="btn btn-primary mg-b-20">
                    <i class="fa fa-unlock-alt {{(app()->getLocale() == 'ar') ? 'mg-l-10' : 'mg-r-10' }}"></i>
                    {{ __('users/profile.Form_changepass')}}
                </a>
            @endif
            

            <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">{{ __('users/profile.Form_NAME')}}</label>
            <p class="tx-inverse mg-b-25">{{$employee->name}}</p>

            <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">{{ __('users/profile.Form_mobile')}}</label>
            <p class="tx-info mg-b-25">{{$employee->mobile_number}}</p>

            <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">{{ __('users/profile.Form_EMAIL_ADDRESS')}}</label>
            <p class="tx-inverse mg-b-25">
                <a href="mailto:{{$employee->email}}" target="_top">{{$employee->email}}</a>
            </p>

            <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">{{ __('users/profile.Form_NATIONALID')}} </label>
            <p class="tx-inverse mg-b-25">{{$employee->national_id}}</p>

            <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">{{ __('users/profile.Form_COUNTRY')}}</label>
            <p class="tx-inverse mg-b-25">{{$employee->country->name}} </p>

            {{-- <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">{{ __('users/profile.Form_CITY')}}</label>
            <p class="tx-inverse mg-b-25">{{$employee->city->name}} </p> --}}


            <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">{{ __('users/profile.Form_TYPE')}}</label>
            <p class="tx-inverse mg-b-50">
                <span class="square-8 rounded-circle bg-dark mg-r-10"></span>
                {{$employee->type->name}}
            </p>

            <h6 class="tx-gray-800 tx-uppercase tx-semibold  {{(app()->getLocale() == 'ar') ? 'tx-20' : 'tx-13' }} mg-b-25">{{ __('users/profile.Form_OTHER_INFORMATION')}}</h6>


            <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">{{ __('users/profile.Form_DEPARTMENT')}} </label>
            <p class="tx-inverse mg-b-25">{{$employee->jobTitle->department->name}}</p>

            <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">{{ __('users/profile.Form_HIRE')}} </label>
            <p class="tx-inverse mg-b-25">{{$employee->start_hire_date}}</p>
            
            <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">{{ __('users/profile.Form_JOB')}} </label>
            <p class="tx-inverse mg-b-25">{{$employee->jobTitle->name}}</p>

            <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">{{ __('users/profile.Form_RANK')}} </label>
            <p class="tx-inverse mg-b-25">
                @if ($employee->rank == 'Team Leader')
                    {{ __('users/profile.FORM_rank1')}}
                @elseif($employee->rank == 'Senior')
                    {{ __('users/profile.FORM_rank2')}}
                @elseif($employee->rank == 'Middle')
                    {{ __('users/profile.FORM_rank3')}}
                @elseif($employee->rank == 'Junior')
                    {{ __('users/profile.FORM_rank4')}}
                @endif
            </p>


            <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">{{ __('users/profile.Form_EXPERIANCE_YEARS')}} </label>
            <p class="tx-inverse mg-b-25">{{$employee->years_of_experience}}</p>

            <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">{{ __('users/profile.Form_INSURANCE_NUMBER')}} </label>
            <p class="tx-inverse mg-b-25">{{$employee->social_insurance_Number}}</p>

            <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">{{ __('users/profile.Form_SOCIAL_STATUS')}} </label>
            <p class="tx-inverse mg-b-25">
                    {{ __('users/profile.Form_Bread_Winner')}}:
                <span class="label ">
                    {{($employee->socialStatus->is_bread_winner) ? 'True' : 'False'}}
                </span> 
            </p>

          <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-5">Skills</label>
          <a href="{{route('employee.create.skill',$employee->id)}}" class="btn btn-dark mg-b-20">
              <i class="menu-item-icon icon ion-ios-color-wand tx-18 {{(app()->getLocale() == 'ar') ? 'mg-l-10' : 'mg-r-10' }}"></i>
              {{ __('users/profile.Form_ADDSKILL')}}
          </a>
          <ul class="list-unstyled profile-skills skills small">
            @if ($employee->userSkills->count())
                @foreach ($employee->userSkills as $userSkill)
                    <li><span>{{$userSkill->skill->name}}</span></li>
                @endforeach
            @endif
                {{-- <p>No Skills</p> --}}
           
           
          </ul>
        </div><!-- card -->

       
      </div><!-- col-lg-4 -->