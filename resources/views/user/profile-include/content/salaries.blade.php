<div class=" rounded table-responsive contracts">
            @if (auth()->user()->type_id == 3)
                @if ($employee->hasActiveContract()->first())
                <a href="{{route('create.salary',$employee->id)}}" class=" mg-b-25 btn btn-danger pd-x-30 pd-y-15 tx-uppercase tx-bold {{(app()->getLocale() == 'ar') ? 'tx-spacing-1' : 'tx-spacing-6' }}  tx-12">
                    <i class="fa fa-plus {{(app()->getLocale() == 'ar') ? 'mg-l-10' : 'mg-r-10' }} "></i>
                    {{ __('users/profile.salary_create')}}
                </a>
                @else
                <a href="{{route('create.contract')}}" class=" mg-b-25 btn btn-danger pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12">
                        <i class="fa fa-plus mg-r-10"></i>
                        {{ __('users/profile.salary_create_contract')}}
                </a>
                @endif
            @endif
            <br>
            @if($salaries->count())
                @foreach($salaries->chunk(2) as $chunk)
                <div class="row">
                    @foreach ($chunk as $salary)
                    <div class="col-sm-6 col-lg-6">
                            <div class="card shadow-base bd-0">
                                <div class="card-header bg-transparent d-flex justify-content-between align-items-center">
                                    <h6 class="card-title tx-uppercase tx-10 mg-b-0">{{ __('users/profile.salary_id')}}</h6>
                                    <span class="tx-12 tx-uppercase">{{$salary->id}}</span>
                                </div><!-- card-header -->
                                <div class="card-header bg-transparent d-flex justify-content-between align-items-center">
                                    <h6 class="card-title tx-uppercase tx-10 mg-b-0"> {{ __('users/profile.salary_CONTRACT_ID')}}</h6>
                                    <span class="tx-12 tx-uppercase">{{$salary->contract_id}}</span>
                                </div><!-- card-header -->
                                <div class="card-header bg-transparent d-flex justify-content-between align-items-center">
                                    <h6 class="card-title tx-uppercase tx-10 mg-b-0">{{ __('users/profile.salary_DATE')}}</h6>
                                    <span class="tx-12 tx-uppercase">{{$salary->date}}</span>
                                </div><!-- card-header -->
                                <div class="card-body">
                                    <div class="row align-items-center mg-t-5">
                                        <div class="col-6 tx-12">{{ __('users/profile.salary_INCENTIVE')}}</div><!-- col-3 -->
                                        <p class="col-6 mg-b-0 tx-sm"><span class="tx-success"><i class="fa fa-arrow-up"></i> {{($salary->incentive) ? number_format($salary->incentive) : number_format(0.00,2)}}</span></p>  
                                        <div class="col-6 tx-12">{{ __('users/profile.salary_DEDUCTION')}}</div><!-- col-3 -->
                                        <p class="col-6 mg-b-0 tx-sm"><span class="tx-danger"><i class="fa fa-arrow-down"></i> {{($salary->deduction) ? number_format($salary->deduction) :number_format(0.00,2)}}</span></p>
                                    </div>
                                                
                                    <p class=" {{(app()->getLocale() == 'ar') ? 'tx-11' : 'tx-10' }}  ">{{ __('users/profile.salary_OTHER')}}  : {{ number_format($salary->other) }} {{$salary->contract->currancy}}</p>
                                    <p class=" {{(app()->getLocale() == 'ar') ? 'tx-11' : 'tx-10' }}  ">{{ __('users/profile.salary_ALLOWANCE')}} : {{ number_format($salary->allowance) }} {{$salary->contract->currancy}}</p>
                                    <p class=" {{(app()->getLocale() == 'ar') ? 'tx-11' : 'tx-10' }}  ">{{ __('users/profile.salary_TAX')}} : {{ number_format($salary->tax) }} {{$salary->contract->currancy}}</p>
                                    <p class=" {{(app()->getLocale() == 'ar') ? 'tx-11' : 'tx-10' }}  ">{{ __('users/profile.salary_LOAN')}} : {{ number_format($salary->loan) }} {{$salary->contract->currancy}}</p>
                                    <p class=" {{(app()->getLocale() == 'ar') ? 'tx-11' : 'tx-10' }}  ">{{ __('users/profile.salary_net')}} : {{ number_format($salary->Net) }} {{$salary->contract->currancy}}</p>
                                    <h4 class="mg-b-0 tx-inverse tx-lato tx-bold"> {{ __('users/profile.salary_total')}} : {{ number_format($salary->total) }}{{$salary->contract->currancy}}</h4>
                                </div><!-- card-body -->
                                <div class="card-footer tx-right">
                                        <a href="{{route('salary.edit',$salary->id)}}"  class="btn btn-primary btn-icon mg-r-5"><div><i class="fa fa-edit"></i></div></a>
                                        <a href="#" data-salary_id="{{$salary->id}}" class="btn btn-danger btn-icon mg-r-5 btnDeleteSalary"><div><i class="fa fa-times-circle"></i></div></a>
                                </div><!-- card-footer -->
                            </div><!-- card -->
                    </div>
                    @endforeach
                </div>
                @endforeach
            @else
            No salary
            @endif
</div>
    
    
    
    