<div class="row no-gutters">
        {{-- employee id --}}
        <div class="col-md-4">
            <div class="form-group ">
                <label class="form-control-label">
                    {{ __('users/skill.FORM_NAME')}}:
                    <span class="tx-danger">*</span>
                </label>
                <input disabled class="form-control is-invalid" value="{{$employee->name}}" type="text">
                <input type="hidden" name="user_id" value="{{$employee->id}}">
            </div>
        </div>
    
    
        {{-- Skill --}}
        <div class="col-md-4 mg-t--1 mg-md-t-0">
            <div class="form-group mg-md-l--1">
                <label class="form-control-label">
                    {{ __('users/skill.FORM_SKILL')}}:
                    <span class="tx-danger">*</span>
                </label>
                <select name="skill" id="" class="form-control select2-hidden-accessible" data-placeholder="Choose Type" tabindex="-1" aria-hidden="true">
                    <optgroup label="select skill">
                        @foreach ($skills as $skill)
                        <option value="{{$skill->id}}" {{ (old('skill') == $skill->id )?'selected' :'' }} >{{$skill->name}}</option>
                        @endforeach
                    </optgroup>
                </select>
                <span class="form-error">
                        <small>{{ $errors->first('skill') }}</small>
                </span>
            </div>
        </div>
    
    
    
         {{-- skillLevels --}}
         <div class="col-md-4 mg-t--1 mg-md-t-0">
                <div class="form-group mg-md-l--1">
                    <label class="form-control-label">
                        {{ __('users/skill.FORM_LEVEL')}}:
                        <span class="tx-danger">*</span>
                    </label>
                    <select name="skill_level" id="" class="form-control select2-hidden-accessible" data-placeholder="Choose Type" tabindex="-1" aria-hidden="true">
                        <optgroup label="select level">
                            @foreach ($skillLevels as $level)
                            <option value="{{$level->id}}" {{ (old('skill_level') == $level->id )?'selected' :'' }} >{{$level->level_code}}</option>
                            @endforeach
                        </optgroup>
                    </select>
                    <span class="form-error">
                        <small>{{ $errors->first('skill_level') }}</small>
                </span>
                </div>
        </div>


        {{-- Date Acquired --}}
        <div class="col-md-12 ">
            <div class="form-group  bd-t-0-force ">
                <label class="form-control-label {{ ($errors->has('date_acquired')) ? ' is-invalid' : '' }}">
                    {{ __('users/skill.FORM_DATE')}}:
                    <span class="tx-danger">*</span>
                </label>
                <input class="form-control" type="date" value="{{old('date_acquired')}}" name="date_acquired" required>
                <span class="form-error">
                        <small>{{ $errors->first('date_acquired') }}</small>
                </span>
            </div>
        </div>
    
    </div>
    <!-- row -->
    
    
    {{-- buttons --}}
    <div class="form-layout-footer bd pd-20 bd-t-0">
        <button type="submit" class="btn btn-info">{{ __('users/skill.CREAT_BUTTON')}}</button>
        <a href="{{route('employee',$employee->id)}}" class="btn btn-secondary">{{ __('users/skill.CREAT_CANCEL')}}</a> 
    </div>
    <!-- form-group -->