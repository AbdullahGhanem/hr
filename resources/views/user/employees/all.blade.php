@extends('master')


@section('title')
    All Employees 
@endsection

{{-- @section('sub-nav')
    <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item active" href="{{route('users')}}">Employees</a>
        </nav>
      </div><!-- br-pageheader -->
@endsection --}}

@section('content')
        
        <div class="pd-x-30 pd-t-30">
          <h4 class="tx-gray-800 mg-b-5">{{ __('users/employees.header')}}</h4>
          <p class="mg-b-0"></p>
        </div>

       
        


        <div class="br-pagebody">
            <div class="br-section-wrapper">
                <div class="br-pagebody pd-x-20 pd-sm-x-30">
                    <h6 class="tx-inverse tx-uppercase tx-bold tx-14 ">{{ __('users/employees.USERS_TABLE')}}</h6>
                    <p class="mg-b-25 mg-lg-b-50">{{ __('users/employees.SUB_TITLE_USERS_TABLE')}}</p>
                    <a  href="{{route('create.employee')}}"
                        class=" mg-b-25 btn btn-danger pd-x-30 pd-y-15 tx-uppercase tx-bold {{(app()->getLocale() == 'ar') ? 'tx-spacing-1' : 'tx-spacing-6 ' }} tx-12" 
                        style="color:white">
                        <i class="fa fa-plus {{(app()->getLocale() == 'ar') ? 'mg-l-10' : 'mg-r-10' }}"></i>
                        {{ __('users/employees.ADD_EMPLOYEE_BUTTON')}}
                    </a>

                    <div class="row">
                            <div class="col-md-10">
                                <form action="{{route('employees')}}" method="get" class="form-inline">
                                    <div class="form-group {{(app()->getLocale() == 'ar') ? 'mg-l-30' : 'mg-r-30' }}">
                                        <input type="text" value="{{ request('name')}}" class="form-control" name="name" placeholder="{{__('users/employees.SEARCH_EMPLOYEE_NAME')}}">
                                    </div>
                                    <div class="form-group {{(app()->getLocale() == 'ar') ? 'mg-l-30' : 'mg-r-30' }}">
                                            <input type="text" value="{{ request('email')}}" class="form-control" name="email" placeholder="{{__('users/employees.SEARCH_EMPLOYEE_EMAIL')}}">
                                    </div>
                                    <div class="form-group {{(app()->getLocale() == 'ar') ? 'mg-l-30' : 'mg-r-30' }}">
                                            <input type="text" value="{{ request('national_id')}}" class="form-control" name="national_id" placeholder="{{__('users/employees.SEARCH_EMPLOYEE_NATIONAL_ID')}}">
                                    </div>
                                    <div class="form-group mg-t-10 {{(app()->getLocale() == 'ar') ? 'mg-l-30' : 'mg-r-30' }}">
                                            <input type="text" value="{{ request('supervisor')}}" class="form-control" name="supervisor" placeholder="{{__('users/employees.SEARCH_SUPER_NAME')}}">
                                    </div>
                                    <div class="form-group mg-t-10 {{(app()->getLocale() == 'ar') ? 'mg-l-30' : 'mg-r-30' }}">
                                        <input type="text" value="{{ request('dept_name')}}" class="form-control" name="dept_name" placeholder="{{__('users/employees.SEARCH_DEPT_NAME')}}">
                                    </div>
                                    <div class="form-group mg-t-10 {{(app()->getLocale() == 'ar') ? 'mg-l-10' : 'mg-r-10' }}">
                                        <input type="text" value="{{ request('jopTitle')}}" class="form-control" name="jopTitle" placeholder="{{__('users/employees.SEARCH_JOB_NAME')}}">
                                    </div>
                                    <div class="form-group mg-t-10 ">
                                        <button type="submit" class="btn btn-primary">{{__('users/employees.SEARCH_SEARCH_BTN')}}</button>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-2">
                                <a href="{{route('employees')}}" class="btn btn-dark">{{__('users/employees.SEARCH_CLEAR_SEARCH_BTN')}}</a>
                            </div>
                        </div>
                        <hr>

                    <div class=" rounded table-responsive">
                        <table class="table table-striped table-bordered table-hover mg-b-0">
                            <thead class="thead-colored thead-dark">
                                <tr>
                                    <td>{{__('users/employees.TABLE_HEAD_NAME')}}</td>
                                    <td>{{__('users/employees.TABLE_HEAD_SUPER')}}</td>
                                    <td>{{__('users/employees.TABLE_HEAD_DEPT')}}</td>
                                    <td>{{__('users/employees.TABLE_HEAD_OPTIONS')}}</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($employees as $employee)
                                    <tr>
                                        <td class="user-info-table">
                                            <a href="{{route('employee',$employee->id)}}">
                                                @if ($employee->image == null)
                                                    <img src="{{asset('img/6.jpg')}}" class="user-image rounded-circle" alt="">
                                                @else
                                                    <img src="{{ asset('storage/images/users/'. $employee->image) }}" class="user-image rounded-circle" alt="">
                                                @endif
                                                {{$employee->name}}
                                            </a>  
                                            <div class="user-info" >
                                                <small><span class="label ">{{__('users/employees.TABLE_JOBTITLE')}}</span>  : {{$employee->jobTitle->name}}</small> 
                                            </div>
                                            <div class="user-info" >
                                                @if ($employee->type->name_en == 'employee')
                                                <span class="square-8 rounded-circle bg-dark mg-r-10"></span>
                                                @elseif($employee->type->name_en == 'admin')
                                                <span class="square-8 rounded-circle bg-warning mg-r-10"></span>
                                                @elseif($employee->type->name_en == 'superAdmin')
                                                <span class="square-8 rounded-circle bg-danger mg-r-10"></span>
                                                @endif
                                                <small>{{$employee->type->name}} </small>
                                            </div> 
                                        </td>
                                        <td class="user-info-table">
                                            @if (!is_null( $employee->supervisor) )
                                                @if ($employee->supervisor['image'] != null)
                                                    <img src="{{ asset('storage/images/users/'. $employee->supervisor['image']) }}" class="user-image rounded-circle" alt="">
                                                @endif
                                                {{$employee->supervisor['name']}}
                                                <div class="user-info" >
                                                        <small><span class="label label-danger ">{{__('users/employees.TABLE_Email')}}</span>  : {{$employee->supervisor['email']}}</small> 
                                                </div>
                                            @endif
                                        </td>
                                        <td>{{$employee->jobTitle->department->name}}</td>
                                        <td class="options">
                                            @if (Auth::user()->id == $employee->id || Auth::user()->type->id == 3)
                                                <a href="{{route('user.edit',$employee->id)}}">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            @endif
                                            @if ( Auth::user()->type->id == 3)
                                                <a href="" class="btnDelete" data-user_id="{{$employee->id}}">
                                                    <i class="fa fa-trash"></i>               
                                                </a>  
                                            @endif               
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{ $employees->appends($_GET)->links() }}
                </div> 
            </div>
        </div>


        @include('include.modals.delete')
        @endsection
        
        @section('script')
                  <script src="{{asset('js/delete-config.js')}}"></script>
                  <script src="{{asset('js/user/delete.js')}}"></script>
        @endsection






