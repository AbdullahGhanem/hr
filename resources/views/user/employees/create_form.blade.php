<div class="row no-gutters">
    {{-- Name EN --}}
    <div class="col-md-4">
        <div class="form-group ">
            <label class="form-control-label {{ ($errors->has('name_en')) ? ' is-invalid' : '' }}">
                {{ __('users/create.FORM_NAME_EN')}}:
                <span class="tx-danger">*</span>
            </label>
            <input class="form-control is-invalid" value="{{old('name_en')}}" type="text" name="name_en"   required placeholder="{{ __('users/create.FORM_NAME_EN_PLACEHOLDER')}}">
            <span class="form-error">
                <small>{{ $errors->first('name_en') }}</small>
            </span>
        </div>
    </div>



    {{-- Name AR --}}
    <div class="col-md-4 mg-t--1 mg-md-t-0">
        <div class="form-group mg-md-l--1">
            <label class="form-control-label {{ ($errors->has('name_ar')) ? ' is-invalid' : '' }} ">
                {{ __('users/create.FORM_NAME_AR')}}:
                <span class="tx-danger">*</span>
            </label>
            <input class="form-control" type="text" value="{{old('name_ar')}}" name="name_ar"   required placeholder="{{ __('users/create.FORM_NAME_AR_PLACEHOLDER')}} ">
            <span class="form-error">
                    <small>{{ $errors->first('name_ar') }}</small>
            </span>
        </div>
    </div>
    
    

    {{-- Email address --}}
    <div class="col-md-4 mg-t--1 mg-md-t-0">
        <div class="form-group mg-md-l--1">
            <label class="form-control-label {{ ($errors->has('email')) ? ' is-invalid' : '' }}">
                {{ __('users/create.FORM_EMAIL_ADDRESS')}}:
                <span class="tx-danger">*</span>
            </label>
            <input class="form-control" type="text" name="email" value="{{old('email')}}"  required placeholder="{{ __('users/create.FORM_EMAIL_ADDRESS_PLACEHOLDER')}}">
            <span class="form-error">
                    <small>{{ $errors->first('email') }}</small>
            </span>
        </div>
    </div>

    
    

    {{-- Mail address --}}
    <div class="col-md-8">
        <div class="form-group bd-t-0-force">
            <label class="form-control-label {{ ($errors->has('address')) ? ' is-invalid' : '' }}">
                {{ __('users/create.FORM_MAIL_ADDRESS')}}:
                <span class="tx-danger">*</span>
            </label>
            <input class="form-control" type="text" value="{{old('address')}}" name="address"  required placeholder="{{ __('users/create.FORM_MAIL_ADDRESS_PLACEHOLDER')}}">
            <span class="form-error">
                    <small>{{ $errors->first('address') }}</small>
            </span>
        </div>
    </div>

    
    
    {{-- Vacation days --}}
    <div class="col-md-4 mg-t--1 mg-md-t-0">
        <div class="form-group mg-md-l--1 bd-t-0-force">
            <label class="form-control-label {{ ($errors->has('total_vacation_days')) ? ' is-invalid' : '' }}">
                {{ __('users/create.FORM_VACATION_DAYS')}}:
                <span class="tx-danger">*</span>
            </label>
            <input class="form-control" type="number" value="21" name="total_vacation_days"  required placeholder="{{ __('users/create.FORM_VACATION_DAYS_PLACEHOLDER')}}">
            <span class="form-error">
                    <small>{{ $errors->first('total_vacation_days') }}</small>
            </span>
        </div>
    </div>

    {{-- country --}}
    <div class="col-md-4">
        <div class="form-group bd-t-0-force">
            <label class="form-control-label mg-b-0-force {{ ($errors->has('country')) ? ' is-invalid' : '' }}">
                {{ __('users/create.FORM_CITY')}}:
                <span class="tx-danger">*</span>
            </label>
            <select name="country" class="form-control select2-hidden-accessible" data-placeholder="{{ __('users/create.FORM_CITY_PLACEHOLDER')}}" tabindex="-1" aria-hidden="true">
                <option label="{{ __('users/create.FORM_CITY_PLACEHOLDER')}}" selected></option>
                @foreach ($countries as $country )
                 <option value="{{$country->id}}" {{ (old('country') == $country->id )?'selected' :'' }}>{{$country->name}}</option>
                @endforeach
               
            </select>
            <span class="form-error">
                    <small>{{ $errors->first('city') }}</small>
            </span>
        </div>
    </div>

    {{-- National ID --}}
    <div class="col-md-8">
        <div class="form-group mg-md-l--1 bd-t-0-force">
            <label class="form-control-label {{ ($errors->has('national_id')) ? ' is-invalid' : '' }}">
                {{ __('users/create.FORM_NATIONAL_ID')}}
                <span class="tx-danger">*</span>
            </label>
            <input class="form-control" type="number" value="{{old('national_id')}}" name="national_id" required placeholder="{{ __('users/create.FORM_NATIONAL_ID_PLACEHOLDER')}}">
            <span class="form-error">
                    <small>{{ $errors->first('national_id') }}</small>
            </span>
        </div>
    </div>


    {{-- type --}}
    <div class="col-md-4">
        <div class="form-group bd-t-0-force">
            <label class="form-control-label mg-b-0-force {{ ($errors->has('employee_type')) ? ' is-invalid' : '' }}">
                {{ __('users/create.FORM_TYPE')}}:
                <span class="tx-danger">*</span>
            </label>
            <select name="employee_type" class="form-control select2-hidden-accessible" data-placeholder="Choose Type" tabindex="-1" aria-hidden="true">
                <option label="{{ __('users/create.FORM_TYPE_PLACEHOLDER')}}" selected></option>
                @foreach ($types as $type )
                    <option value="{{$type->id}}" {{ (old('employee_type') == $type->id )?'selected' :'' }} >{{$type->name}}</option>
                @endforeach

            </select>
            <span class="form-error">
                    <small>{{ $errors->first('employee_type') }}</small>
            </span>
        </div>
    </div>





    {{-- Gender --}}
    <div class="col-md-4 mg-t--1 mg-md-t-0">
        <div class="form-group mg-md-l--1 bd-t-0-force">
            <label class="form-control-label {{ ($errors->has('gender')) ? ' is-invalid' : '' }}">
                {{ __('users/create.FORM_GENDER')}}:
                <span class="tx-danger">*</span>
            </label>
            <select name="gender" class="form-control select2-hidden-accessible" data-placeholder="Choose Gender" tabindex="-1" aria-hidden="true">
                <option label="{{ __('users/create.FORM_GENDER_PLACEHOLDER')}}" selected></option>
                <option value="M" {{ (old('gender') == 'M')?'selected' :'' }}>{{ __('users/create.FORM_GENDER_MALE')}}</option>
                <option value="F" {{ (old('gender') == 'F')?'selected' :'' }}>{{ __('users/create.FORM_GENDER_FEMALE')}}</option>
            </select>
            <span class="form-error">
                    <small>{{ $errors->first('gender') }}</small>
            </span>
        </div>
    </div>


    {{-- Mobile Number --}}
    <div class="col-md-4 ">
        <div class="form-group mg-md-l--1 bd-t-0-force">
            <label class="form-control-label {{ ($errors->has('mobile_number')) ? ' is-invalid' : '' }}">
                {{ __('users/create.FORM_MOBILE')}}:
                <span class="tx-danger">*</span>
            </label>
            <input class="form-control" type="text" value="{{old('mobile_number')}}" name="mobile_number" required  placeholder="{{ __('users/create.FORM_MOBILE_PLACEHOLDER')}}">
            <span class="form-error">
                    <small>{{ $errors->first('mobile_number') }}</small>
            </span>
        </div>
    </div>



    {{-- job_title_id --}}
    <div class="col-md-4">
        <div class="form-group bd-t-0-force">
            <label class="form-control-label mg-b-0-force {{ ($errors->has('job_title_id')) ? ' is-invalid' : '' }}">
                {{ __('users/create.FORM_JOB')}}:
                <span class="tx-danger">*</span>
            </label>
            <select name="job_title_id" class="form-control select2-hidden-accessible" data-placeholder="Choose Job title" tabindex="-1"
                aria-hidden="true">
                <option label="{{ __('users/create.FORM_JOB_PLACEHOLDER')}}" selected></option>
                @foreach ($jobTitles as $jobTitle)
                <option value="{{$jobTitle->id}}" {{ (old('job_title_id') == $jobTitle->id )?'selected' :'' }}>{{$jobTitle->name}}</option>
                @endforeach
            </select>
            <span class="form-error">
                    <small>{{ $errors->first('job_title_id') }}</small>
            </span>
        </div>
    </div>


    {{-- Social Insurance Number --}}
    <div class="col-md-8">
        <div class="form-group mg-md-l--1 bd-t-0-force">
            <label class="form-control-label {{ ($errors->has('social_insurance_Number')) ? ' is-invalid' : '' }}">
                {{ __('users/create.FORM_SOCIAL_INSURANCE_NUMBER')}} :
                <span class="tx-danger">*</span>
            </label>
            <input class="form-control" type="number" value="{{old('social_insurance_Number')}}" name="social_insurance_Number" required  placeholder=" {{ __('users/create.FORM_SOCIAL_INSURANCE_NUMBER_PLACEHOLDER')}}">
            <span class="form-error">
                <small>{{ $errors->first('social_insurance_Number') }}</small>
            </span>
        </div>
    </div>
    
    
    
    {{-- image --}}
    <div class="col-md-4">
        <div class="form-group bd-t-0-force">
            <label class="custom-file">
                <input name="image" type="file" id="file" class="custom-file-input">
                <span class="custom-file-control custom-file-control-primary"></span>
            </label>
            
        </div>
    </div>
    
    
    
    {{-- dob --}}
    <div class="col-md-4 mg-t--1 mg-md-t-0">
        <div class="form-group mg-md-l--1 bd-t-0-force">
            <label class="form-control-label {{ ($errors->has('dob')) ? ' is-invalid' : '' }}">
                    {{ __('users/create.FORM_DATE_OF_BIRTH')}} :
                <span class="tx-danger">*</span>
            </label>
            <input class="form-control" type="date" value="{{old('dob')}}" name="dob" required placeholder="Enter Date of birth ">
            <span class="form-error">
                    <small>{{ $errors->first('dob') }}</small>
            </span>
        </div>
    </div>

    
    

    {{-- years_of_experience --}}
    <div class="col-md-4 ">
        <div class="form-group mg-md-l--1 bd-t-0-force">
            <label class="form-control-label {{ ($errors->has('years_of_experience')) ? ' is-invalid' : '' }}">
                    {{ __('users/create.FORM_YEARS_OF_EXPERIENCE')}}:
                <span class="tx-danger">*</span>
            </label>
            <input class="form-control" type="number" value="{{old('years_of_experience')}}" name="years_of_experience" value="0" required placeholder="{{ __('users/create.FORM_YEARS_OF_EXPERIENCE_PLACEHOLDER')}}">
            <span class="form-error">
                    <small>{{ $errors->first('years_of_experience') }}</small>
            </span>
        </div>
    </div>



    {{-- supervisor --}}
    <div class="col-md-3">
        <div class="form-group bd-t-0-force">
            <label class="form-control-label {{ ($errors->has('supervisor_id')) ? ' is-invalid' : '' }}">
                    {{ __('users/create.FORM_SUPERVISOR')}}:
                <span class="tx-danger">*</span>
            </label>
            <select name="supervisor_id" class="form-control select2" data-placeholder="Choose Browser">
                <option label="{{ __('users/create.FORM_SUPERVISOR_PLACEHOLDER')}}" selected></option>
                @foreach ($employees as $employee)
                    <option value="{{$employee->id}}" {{ (old('supervisor_id') == $employee->id )?'selected' :'' }}>{{$employee->name}}</option>
                @endforeach
            </select>
            <span class="form-error">
                    <small>{{ $errors->first('supervisor_id') }}</small>
            </span>
        </div>
    </div>



    {{-- start_hire_date --}}
    <div class="col-md-3 mg-t--1 mg-md-t-0">
        <div class="form-group mg-md-l--1 bd-t-0-force">
            <label class="form-control-label {{ ($errors->has('start_hire_date')) ? ' is-invalid' : '' }}">
                    {{ __('users/create.FORM_Start_Hiring_Date')}}:
                    <span class="tx-danger">*</span>
                </label>
                <input class="form-control" type="date" value="{{old('start_hire_date')}}" name="start_hire_date" required placeholder="Enter >Start Hiring Date">
                <span class="form-error">
                    <small>{{ $errors->first('start_hire_date') }}</small>
                </span>
            </div>
        </div>
        
        
        
        {{-- Rank --}}
        <div class="col-md-3 ">
            <div class="form-group mg-md-l--1 bd-t-0-force">
                <label class="form-control-label {{ ($errors->has('rank')) ? ' is-invalid' : '' }}">
                        {{ __('users/create.FORM_rank')}}:
                        <span class="tx-danger">*</span>
                    </label>
                    <select name="rank" class="form-control select2" data-placeholder="Choose Browser">
                        <option label=" {{ __('users/create.FORM_rank_PLACEHOLDER')}}" selected></option>    
                        <option value="T" {{ (old('rank') == 'T' )?'selected' :'' }}>{{__('users/create.FORM_rank1')}}</option>
                        <option value="S" {{ (old('rank') == 'S' )?'selected' :'' }}>{{__('users/create.FORM_rank2')}}</option>
                        <option value="M" {{ (old('rank') == 'M' )?'selected' :'' }}>{{__('users/create.FORM_rank3')}}</option>
                        <option value="J" {{ (old('rank') == 'J' )?'selected' :'' }}>{{__('users/create.FORM_rank4')}}</option>
                    </select>
                    <span class="form-error">
                        <small>{{ $errors->first('rank') }}</small>
                    </span>
                </div>
            </div>
            
            
            {{-- is_bread_winner --}}
            <div class="col-md-3 mg-t--1 mg-md-t-0">
                <div class="form-group mg-md-l--1 bd-t-0-force">
                    <label class="form-control-label {{ ($errors->has('is_bread_winner')) ? ' is-invalid' : '' }}">
                        {{ __('users/create.FORM_breadwinner')}}:
                    <span class="tx-danger">*</span>
                </label>
                <label class="switch">
                    <input type="checkbox" name="is_bread_winner" checked>
                    <span class="slider round"></span>
                </label>
                <span class="form-error">
                    <small>{{ $errors->first('is_bread_winner') }}</small>
                </span>
            </div>
    </div>




</div>
<!-- row -->


{{-- buttons --}}
<div class="form-layout-footer bd pd-20 bd-t-0">
    <button class="btn btn-info">{{ __('users/create.CREAT_BUTTON')}}</button>
    <a href="{{route('employees')}}" class="btn btn-secondary">{{ __('users/create.CREAT_CANCEL')}}</a>
</div>
<!-- form-group -->