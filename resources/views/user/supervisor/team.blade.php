@extends('master')


@section('title')
    All Supervisor Team 
@endsection

{{-- @section('sub-nav')
    <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item active" href="{{route('users')}}">Users</a>
        </nav>
      </div><!-- br-pageheader -->
@endsection --}}

@section('content')
        
        <div class="pd-x-30 pd-t-30">
          <h4 class="tx-gray-800 mg-b-5">{{ __('users/supervisor.header')}}</h4>
          <p class="mg-b-0"></p>
        </div>

       
        


        <div class="br-pagebody">
            <div class="br-section-wrapper">
                <div class="br-pagebody pd-x-20 pd-sm-x-30">
                    <h6 class="tx-inverse tx-uppercase tx-bold tx-14 ">{{ __('users/supervisor.USERS_TABLE')}}</h6>
                    <p class="mg-b-25 mg-lg-b-50">
                        {{ __('users/supervisor.SUB_TITLE_USERS_TABLE1')}}
                        <span class="label label-danger ">{{$employee->name}}</span>
                        {{ __('users/supervisor.SUB_TITLE_USERS_TABLE2')}}
                    </p>




                    <div class=" rounded table-responsive">
                        <table class="table table-striped table-bordered table-hover mg-b-0">
                            <thead class="thead-colored thead-dark">
                                <tr>
                                    <td>{{__('users/supervisor.TABLE_HEAD_NAME')}}</td>
                                    <td>{{__('users/supervisor.TABLE_HEAD_SUPER')}}</td>
                                    <td>{{__('users/supervisor.TABLE_HEAD_DEPT')}}</td>
                                    <td>{{__('users/supervisor.TABLE_HEAD_OPTIONS')}}</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($team as $user)
                                    <tr>
                                        <td class="user-info-table">
                                            <a href="{{route('employee',$user->id)}}">
                                                @if ($user->image == null)
                                                    <img src="{{asset('img/6.jpg')}}" class="user-image rounded-circle" alt="">
                                                @else
                                                    <img src="{{ asset('storage/images/users/'. $user->image) }}" class="user-image rounded-circle" alt="">
                                                @endif
                                                {{$user->name}}
                                            </a>  
                                            <div class="user-info" >
                                                <small><span class="label ">{{__('users/supervisor.TABLE_JOBTITLE')}}</span>  : {{$user->jobTitle->name}}</small> 
                                            </div>
                                            <div class="user-info" >
                                                @if ($user->type->name_en == 'employee')
                                                <span class="square-8 rounded-circle bg-dark mg-r-10"></span>
                                                @elseif($user->type->name_en == 'admin')
                                                <span class="square-8 rounded-circle bg-warning mg-r-10"></span>
                                                @elseif($user->type->name_en == 'superAdmin')
                                                <span class="square-8 rounded-circle bg-danger mg-r-10"></span>
                                                @endif
                                                <small>{{$user->type->name}} </small>
                                            </div> 
                                        </td>
                                        <td class="user-info-table">
                                            @if (!is_null( $user->supervisor) )
                                                @if ($user->supervisor['image'] != null)
                                                    <img src="{{ asset('storage/images/users/'. $user->supervisor['image']) }}" class="user-image rounded-circle" alt="">
                                                @endif
                                                {{$user->supervisor['name']}}
                                                <div class="user-info" >
                                                        <small><span class="label label-danger ">{{__('users/supervisor.TABLE_Email')}}</span>  : {{$user->supervisor['email']}}</small> 
                                                </div>
                                            @endif
                                        </td>
                                        <td>{{$user->jobTitle->department->name}}</td>
                                        <td class="options">
                                            @if (Auth::user()->id == $user->id || Auth::user()->type->id == 3)
                                                <a href="{{route('user.edit',$user->id)}}">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            @endif
                                            @if ( Auth::user()->type->id == 3)
                                                <a href="" class="btnDelete" data-user_id="{{$user->id}}">
                                                    <i class="fa fa-trash"></i>               
                                                </a>  
                                            @endif               
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{ $team->appends($_GET)->links() }}
                </div> 
            </div>
        </div>


        @include('include.modals.delete')
        @endsection
        
        @section('script')
                  <script src="{{asset('js/delete-config.js')}}"></script>
                  <script src="{{asset('js/user/delete.js')}}"></script>
        @endsection






