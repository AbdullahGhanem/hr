<div class="form-layout form-layout-1">
        <div class="row mg-b-25">
          <div class="col-lg-12 mg-b-20">
            <div class="form-group">
              <label class="form-control-label {{ ($errors->has('old_password')) ? ' is-invalid' : '' }}">{{__('users/edit_pass.FORM_OLD')}} <span class="tx-danger">*</span></label>
              <input class="form-control" type="password" name="old_password" placeholder="********" minlength="8" maxlength="32">
              <span class="form-error">
                  <small>{{ $errors->first('old_password') }}</small>
              </span>
            </div>
          </div><!-- col-4 -->
          <div class="col-lg-12 mg-b-20">
            <div class="form-group">
              <label class="form-control-label {{ ($errors->has('new_password')) ? ' is-invalid' : '' }}">{{__('users/edit_pass.FORM_NEW')}}: <span class="tx-danger">*</span></label>
              <input class="form-control" type="password" name="new_password" placeholder="****************" minlength="8" maxlength="32">
              <span class="form-error">
                  <small>{{ $errors->first('new_password') }}</small>
              </span>
            </div>
          </div><!-- col-4 -->
          <div class="col-lg-12 mg-b-20">
            <div class="form-group">
              <label class="form-control-label {{ ($errors->has('password_confirmation')) ? ' is-invalid' : '' }}">{{__('users/edit_pass.FORM_CONFIRM')}}: <span class="tx-danger">*</span></label>
              <input class="form-control" type="password" name="password_confirmation" placeholder="****************" minlength="8" maxlength="32">
              <span class="form-error">
                  <small>{{ $errors->first('password_confirmation') }}</small>
              </span>
            </div>
          </div><!-- col-4 -->
         
         
        </div><!-- row -->

        <div class="form-layout-footer">
          <button type="submit" class="btn btn-info">{{__('users/edit_pass.CREAT_BUTTON')}}</button>
          <a href="{{route('employees')}}" class="btn btn-secondary">{{__('users/edit_pass.CREAT_CANCEL')}}</a>
        </div><!-- form-layout-footer -->
      </div>