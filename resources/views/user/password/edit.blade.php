@extends('master')


@section('title')
    User Change Password
@endsection

{{-- @section('sub-nav')
    <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item active" href="{{route('users')}}">Employees</a>
        </nav>
      </div><!-- br-pageheader -->
@endsection --}}

@section('content')
        
        <div class="pd-x-30 pd-t-30">
          <h4 class="tx-gray-800 mg-b-5">{{__('users/edit_pass.header')}}</h4>
          <p class="mg-b-0"></p>
        </div>

       

        <div class="br-pagebody">
          <div class="br-section-wrapper">
                  <h6 class="tx-gray-800 tx-uppercase tx-bold tx-14 mg-t-80 mg-b-10">{{__('users/edit_pass.SUB_TITLE')}}</h6>
                  <div class="form-layout form-layout-2">
                    <form action="{{route('employee.password.update',$user->id)}}" method="post">
                        {{csrf_field()}}
                        @include('user.password.edit_form')
                    </form>
                  </div>
          </div><!-- br-section-wrapper -->
        </div>


@endsection

@section('script')
@endsection
