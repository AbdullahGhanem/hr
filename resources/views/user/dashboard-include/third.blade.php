<!-- employees -->
<div class=" col-sm-6 col-lg-12  card shadow-base bd-0 mg-t-20">
    <div class="card-header bg-transparent pd-x-25 pd-y-15 bd-b-0 d-flex justify-content-between align-items-center">
      <h6 class="card-title tx-uppercase {{(app()->getLocale() == 'ar') ? 'tx-22' : 'tx-12' }} mg-b-0">{{__('dashboard.TOP_MONTHLY_INSENTIVE')}}</h6>
      {{-- <a href="" class="tx-gray-500 hover-info lh-0"><i class="icon ion-android-more-horizontal tx-24 lh-0"></i></a> --}}
    </div><!-- card-header -->
    <div class="card-body pd-x-25 pd-b-25 pd-t-0">
      <div class="row no-gutters">
        @foreach ($top_employees_salaries as $employee_salary)
            @if ($top_employees_salaries->count() === 4)
              <div class="col-sm-6 col-lg-3 mg-t--1 mg-sm-t-0 mg-lg-l--1">
            @elseif($top_employees_salaries->count() === 3)
              <div class="col-sm-6 col-lg-4 mg-t--1 mg-sm-t-0 mg-lg-l--1">
            @elseif($top_employees_salaries->count() === 2)
              <div class="col-sm-6 col-lg-6 mg-t--1 mg-sm-t-0 mg-lg-l--1">
            @elseif($top_employees_salaries->count() === 1)
              <div class="col-sm-6 col-lg-12 mg-t--1 mg-sm-t-0 mg-lg-l--1">
            @else
              <div class="col-sm-6 col-lg-3 mg-t--1 mg-sm-t-0 mg-lg-l--1">
            @endif
            
              <div class="card card-body rounded-0">
                      <h5 class="tx-inverse {{(app()->getLocale() == 'ar') ? 'tx-18' : 'tx-12' }} mg-b-5">  {{__('dashboard.Department')}} :</h5>
                      <h6 class="tx-inverse {{(app()->getLocale() == 'ar') ? 'tx-18' : 'tx-12' }} mg-b-5">{{$employee_salary->contract->user->jobTitle->department->name}}</h6>
                      <span class="{{(app()->getLocale() == 'ar') ? 'tx-14' : 'tx-10' }}">{{__('dashboard.jobtitle')}} :  {{$employee_salary->contract->user->jobTitle->name}}</span>
                <div class="tx-center mg-y-20">
                  <img class="top-preformance-user" src="{{asset('storage/images/users/'.$employee_salary->contract->user->image)}}" width="170px" height="170px" alt="">
                </div>
                <p class="tx-10 tx-uppercase tx-medium mg-b-0 tx-spacing-1">{{__('dashboard.Employee_Name')}}</p>
                <h6 class="tx-inverse tx-bold tx-lato">
                  <span>{{$employee_salary->contract->user->name}}</span>
                </h6>
                <div class="d-flex justify-content-between tx-12">
                  <div>
                    <span class="square-10 bg-info mg-r-5"></span> {{number_format($employee_salary->incentive)}} {{$employee_salary->contract->currancy}}  {{__('dashboard.INSENTIVE')}}
                    <h5 class="mg-b-0 mg-t-5 tx-bold tx-inverse tx-lato">
                      <span class="label">
                          {{__('dashboard.Total')}} : {{number_format($employee_salary->total)}}  {{$employee_salary->contract->currancy}} 
                      </span>
                        </h5>
                  </div>
                
                </div><!-- d-flex -->
              </div><!-- card -->
            </div><!-- col-3 -->
             
        @endforeach

      </div><!-- row -->
    </div><!-- card-body -->
  </div>