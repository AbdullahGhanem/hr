@extends('master')


@section('title')
    User Dashboard
@endsection

{{-- @section('sub-nav')
    <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="index.html">Bracket</a>
            <a class="breadcrumb-item" href="#">UI ELements</a>
            <span class="breadcrumb-item active">Accordion</span>
        </nav>
      </div><!-- br-pageheader -->
@endsection --}}

@section('content')
        
        <div class="pd-x-30 pd-t-30">
          <h4 class="tx-gray-800 mg-b-5">{{__('dashboard.Dashboard')}}</h4>
          <p class="mg-b-0"></p>
        </div>

        <div class="br-pagebody">
          <div class="tab-content br-profile-body">
            <div class="tab-pane fade active show" id="posts" aria-expanded="true">
              <div class="card pd-20 pd-xs-30 shadow-base bd-0">

                <div class="row">
                  @include('user.dashboard-include.first')
                  @include('user.dashboard-include.second')
                  @include('user.dashboard-include.third')
                </div>


              </div>
            </div>
          </div>
        </div>

@endsection






