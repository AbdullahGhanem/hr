@extends('master')


@section('title')
    All Admins 
@endsection

{{-- @section('sub-nav')
    <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item active" href="{{route('users')}}">Employees</a>
        </nav>
      </div><!-- br-pageheader -->
@endsection --}}

@section('content')
        
        <div class="pd-x-30 pd-t-30">
          <h4 class="tx-gray-800 mg-b-5">{{ __('users/admins.header')}}</h4>
          <p class="mg-b-0"></p>
        </div>

       
        


        <div class="br-pagebody">
            <div class="br-section-wrapper">
                <div class="br-pagebody pd-x-20 pd-sm-x-30">
                    <h6 class="tx-inverse tx-uppercase tx-bold tx-14 ">{{ __('users/admins.USERS_TABLE')}}</h6>
                    <p class="mg-b-25 mg-lg-b-50">{{ __('users/admins.SUB_TITLE_USERS_TABLE')}}</p>


                    <div class="row">
                            <div class="col-md-10">
                                <form action="{{route('admins')}}" method="get" class="form-inline">
                                    <div class="form-group {{(app()->getLocale() == 'ar') ? 'mg-l-30' : 'mg-r-30' }}">
                                        <input type="text" value="{{ request('name')}}" class="form-control" name="name" placeholder="{{__('users/admins.SEARCH_EMPLOYEE_NAME')}}">
                                    </div>
                                    <div class="form-group {{(app()->getLocale() == 'ar') ? 'mg-l-30' : 'mg-r-30' }}">
                                            <input type="text" value="{{ request('email')}}" class="form-control" name="email" placeholder="{{__('users/admins.SEARCH_EMPLOYEE_EMAIL')}}">
                                    </div>
                                    <div class="form-group {{(app()->getLocale() == 'ar') ? 'mg-l-30' : 'mg-r-30' }}">
                                            <input type="text" value="{{ request('national_id')}}" class="form-control" name="national_id" placeholder="{{__('users/admins.SEARCH_EMPLOYEE_NATIONAL_ID')}}">
                                    </div>
                                    <div class="form-group mg-t-10 {{(app()->getLocale() == 'ar') ? 'mg-l-30' : 'mg-r-30' }}">
                                            <input type="text" value="{{ request('supervisor')}}" class="form-control" name="supervisor" placeholder="{{__('users/admins.SEARCH_SUPER_NAME')}}">
                                    </div>
                                    <div class="form-group mg-t-10 {{(app()->getLocale() == 'ar') ? 'mg-l-30' : 'mg-r-30' }}">
                                        <input type="text" value="{{ request('dept_name')}}" class="form-control" name="dept_name" placeholder="{{__('users/admins.SEARCH_DEPT_NAME')}}">
                                    </div>
                                    <div class="form-group mg-t-10 {{(app()->getLocale() == 'ar') ? 'mg-l-10' : 'mg-r-10' }}">
                                        <input type="text" value="{{ request('jopTitle')}}" class="form-control" name="jopTitle" placeholder="{{__('users/admins.SEARCH_JOB_NAME')}}">
                                    </div>
                                    <div class="form-group mg-t-10 ">
                                        <button type="submit" class="btn btn-primary">{{__('users/admins.SEARCH_SEARCH_BTN')}}</button>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-2">
                                <a href="{{route('admins')}}" class="btn btn-dark">{{__('users/admins.SEARCH_CLEAR_SEARCH_BTN')}}</a>
                            </div>
                        </div>
                        <hr>

                    <div class=" rounded table-responsive">
                        <table class="table table-striped table-bordered table-hover mg-b-0">
                            <thead class="thead-colored thead-dark">
                                <tr>
                                    <td>{{__('users/admins.TABLE_HEAD_NAME')}}</td>
                                    <td>{{__('users/admins.TABLE_HEAD_SUPER')}}</td>
                                    <td>{{__('users/admins.TABLE_HEAD_DEPT')}}</td>
                                    <td>{{__('users/admins.TABLE_HEAD_OPTIONS')}}</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($admins as $admin)
                                    <tr>
                                        <td class="user-info-table">
                                                <a href="{{route('employee',$admin->id)}}">
                                                @if ($admin->image == null)
                                                    <img src="{{asset('img/6.jpg')}}" class="user-image rounded-circle" alt="">
                                                @else
                                                    <img src="{{ asset('storage/images/users/'. $admin->image) }}" class="user-image rounded-circle" alt="">
                                                @endif
                                                {{$admin->name}}
                                                </a>
                                                <div class="user-info" >
                                                       <small><span class="label ">{{__('users/admins.TABLE_JOBTITLE')}}</span>  : {{$admin->jobTitle->name_en}}</small> 
                                                </div>
                                                <div class="user-info" >
                                                        @if ($admin->type->name_en == 'employee')
                                                        <span class="square-8 rounded-circle bg-dark mg-r-10"></span>
                                                        @elseif($admin->type->name_en == 'admin')
                                                        <span class="square-8 rounded-circle bg-warning mg-r-10"></span>
                                                        @elseif($admin->type->name_en == 'superAdmin')
                                                        <span class="square-8 rounded-circle bg-danger mg-r-10"></span>
                                                        @endif
                                                    <small>{{$admin->type->name}} </small>
                                                </div>   
                                        </td>
                                        <td class="user-info-table">
                                            @if (!is_null( $admin->supervisor) )
                                                @if ($admin->supervisor['image'] != null)
                                                    <img src="{{ asset('storage/images/users/'. $admin->supervisor['image']) }}" class="user-image rounded-circle" alt="">
                                                @endif
                                                {{$admin->supervisor['name']}}
                                                <div class="user-info" >
                                                        <small><span class="label label-danger ">{{__('users/admins.TABLE_Email')}}</span>  : {{$admin->supervisor['email']}}</small> 
                                                </div>
                                            @endif
                                        </td>
                                        <td>{{$admin->jobTitle->department->name}}</td>
                                        <td class="options">
                                                @if (Auth::user()->id == $admin->id || Auth::user()->type->id == 3)
                                                    <a href="{{route('user.edit',$admin->id)}}">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                @endif
                                                @if ( Auth::user()->type->id == 3)
                                                    <a href="" class="btnDelete" data-user_id="{{$admin->id}}">
                                                        <i class="fa fa-trash"></i>               
                                                    </a>  
                                                @endif           
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{ $admins->appends($_GET)->links() }}
                </div> 
            </div>
        </div>


        @include('include.modals.delete')
        @endsection
        
        @section('script')
                  <script src="{{asset('js/delete-config.js')}}"></script>
                  <script src="{{asset('js/user/delete.js')}}"></script>
        @endsection






