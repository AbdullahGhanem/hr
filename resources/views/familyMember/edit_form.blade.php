<div class="row no-gutters">
        {{-- Name EN --}}
        <div class="col-md-3">
            <div class="form-group ">
                <label class="form-control-label">
                    {{__('familyMember/edit.FORM_EMPLOYEE_NAME')}}:
                    <span class="tx-danger">*</span>
                </label>
                <input disabled class="form-control is-invalid" value="{{$familyMember->socialStatus->user->name}}" type="text" name="name_en">
            </div>
        </div>
    
    
        {{-- Date  --}}
        <div class="col-md-3 mg-t--1 mg-md-t-0">
            <div class="form-group mg-md-l--1">
                <label class="form-control-label {{ ($errors->has('date')) ? ' is-invalid' : '' }}">
                    {{__('familyMember/edit.FORM_dob')}}:
                    <span class="tx-danger">*</span>
                </label>
                <input required class="form-control" type="date" name="date" value="{{$familyMember->dob}}">
                <span class="form-error">
                        <small>{{ $errors->first('date') }}</small>
                </span>
            </div>
        </div>

        {{-- name_en  --}}
        <div class="col-md-3 mg-t--1 mg-md-t-0">
            <div class="form-group mg-md-l--1">
                <label class="form-control-label {{ ($errors->has('name_en')) ? ' is-invalid' : '' }}">
                    {{__('familyMember/edit.FORM_NAME_EN')}}:
                    <span class="tx-danger">*</span>
                </label>
                <input required class="form-control" type="text" name="name_en" value="{{$familyMember->name_en}}" placeholder=" {{__('familyMember/edit.FORM_NAME_EN_PLACEHOLDER')}}">
                <span class="form-error">
                        <small>{{ $errors->first('name_en') }}</small>
                </span>
            </div>
        </div>

        {{-- name_ar  --}}
        <div class="col-md-3 mg-t--1 mg-md-t-0">
                <div class="form-group mg-md-l--1">
                    <label class="form-control-label {{ ($errors->has('name_ar')) ? ' is-invalid' : '' }}">
                        {{__('familyMember/edit.FORM_NAME_AR')}}:
                        <span class="tx-danger">*</span>
                    </label>
                    <input required class="form-control" type="text" name="name_ar" value="{{$familyMember->name_ar}}" placeholder=" {{__('familyMember/edit.FORM_NAME_AR_PLACEHOLDER')}}">
                    <span class="form-error">
                            <small>{{ $errors->first('name_ar') }}</small>
                    </span>
                </div>
            </div>
    
    

        {{-- Email --}}
        <div class="col-md-4 ">
            <div class="form-group bd-t-0-force">
                <label class="form-control-label {{ ($errors->has('email')) ? ' is-invalid' : '' }}">
                    {{__('familyMember/edit.FORM_email')}}:
                    <span class="tx-danger">*</span>
                </label>
                <input required value="{{ $familyMember->email }}" class="form-control" type="email" name="email" placeholder="{{__('familyMember/edit.FORM_EMAIL_ADDRESS_PLACEHOLDER')}}">
                <span class="form-error">
                        <small>{{ $errors->first('email') }}</small>
                </span>
            </div>
        </div>


        {{--  national_id --}}
        <div class="col-md-4 mg-t--1 mg-md-t-0">
                <div class="form-group mg-md-l--1 bd-t-0-force">
                    <label class="form-control-label {{ ($errors->has('national_id')) ? ' is-invalid' : '' }}">
                        {{__('familyMember/edit.FORM_NATIONAL_ID')}}:
                        <span class="tx-danger">*</span>
                    </label>
                    <input required value="{{ $familyMember->national_id }}" class="form-control" type="number" name="national_id" placeholder="{{__('familyMember/edit.FORM_NATIONAL_ID_PLACEHOLDER')}}">
                    <span class="form-error">
                        <small>{{ $errors->first('national_id') }}</small>
                    </span>
                </div>
        </div>

        {{-- Gender --}}
        <div class="col-md-4 mg-t--1 mg-md-t-0">
            <div class="form-group mg-md-l--1 bd-t-0-force">
                <label class="form-control-label {{ ($errors->has('gender')) ? ' is-invalid' : '' }}">
                        {{__('familyMember/edit.FORM_GENDER')}}:
                        <span class="tx-danger">*</span>
                </label>
                <select required name="gender" class="form-control select2-hidden-accessible" data-placeholder="Choose Gender" tabindex="-1" aria-hidden="true">
                    <option label="{{__('familyMember/edit.FORM_GENDER_PLACEHOLDER')}}" selected></option>
                    <option value="M" {{ ($familyMember->gender == 'M')?'selected' :'' }}>{{__('familyMember/edit.FORM_GENDER_MALE')}}</option>
                    <option value="F" {{ ($familyMember->gender == 'F')?'selected' :'' }}>{{__('familyMember/edit.FORM_GENDER_FEMALE')}}</option>
                </select>
                <span class="form-error">
                        <small>{{ $errors->first('gender') }}</small>
                </span>
            </div>
        </div>
    
    
    </div>
    <!-- row -->
    
    
    {{-- buttons --}}
    <div class="form-layout-footer bd pd-20 bd-t-0">
        <button type="submit" class="btn btn-info">{{__('familyMember/edit.CREAT_BUTTON')}}</button>
        <a href="{{route('employee',$familyMember->socialStatus->user->id.'?Family_page')}}" class="btn btn-secondary">{{__('familyMember/edit.CREAT_CANCEL')}}</a>
    </div>
    <!-- form-group -->