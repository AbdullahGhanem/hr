<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' =>'صفحة إضافة الخبرة',
    'SUB_TITLE' => 'إضافة الخبرة',
    'MINI'=>'سيتم إضافة خبرة جديدة لنظام',


    'FORM_EMPLOYEE_NAME' =>'اسم الموظف',
    'FORM_STARTDATE' =>'تاريخ البدء',
    'FORM_END_DATE' =>'تاريخ الانتهاء',
    'FORM_WHERE' =>'أين',
    'FORM_ROLE_ARABIC' =>'دوره باللغة العربية',
    'FORM_ROLE_ENGLISH' =>'دوره باللغة الإنجليزية',
    'FORM_DESCRIPTION_IN_ARABIC' =>'وصف باللغة العربية',
    'FORM_DESCRIPTION_IN_ENGLISH' =>'وصف باللغة الإنجليزية',

    'FORM_WHERE_PLACEHOLDER' =>'أدخل أين',
    'FORM_ROLE_AR_PLACEHOLDER' =>'أدخل  دورة باللغة العربية',
    'FORM_ROLE_EN_PLACEHOLDER' =>'أدخل  دورة باللغة الإنجليزية',
    'FORM_desc_AR_PLACEHOLDER' =>'أدخل  الوصف باللغة العربية',
    'FORM_desc_EN_PLACEHOLDER' =>'أدخل  الوصف باللغة الإنجليزية',

    'CREAT_BUTTON' => 'تقديم البيانات',
    'CREAT_CANCEL' => 'إلغاء',

];
