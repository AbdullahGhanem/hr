<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' =>'صفحة تحديث  عضو في الأسرة',
    'SUB_TITLE' => 'تحديث  عضو في الأسرة',

    'FORM_EMPLOYEE_NAME' =>'اسم الموظف',
    'FORM_dob'=>' تاريخ الولادة',
    'FORM_NAME_EN' =>'الاسم باللغة الانجليزية',
    'FORM_NAME_AR' =>'الاسم باللغة العربية',
    'FORM_email'=>' البريد الإلكتروني',
    'FORM_NATIONAL_ID'=>' الهوية الوطنية ',
    'FORM_GENDER'=>' جنس',


    'FORM_GENDER_MALE'=>'ذكر',
    'FORM_GENDER_FEMALE'=>'أنثى',

    'FORM_NAME_EN_PLACEHOLDER' =>'أدخل الاسم باللغة الإنجليزية',
    'FORM_NAME_AR_PLACEHOLDER' =>'أدخل الاسم باللغة العربية',
    'FORM_EMAIL_ADDRESS_PLACEHOLDER' =>'أدخل عنوان البريد الالكتروني',
    'FORM_NATIONAL_ID_PLACEHOLDER' =>'أدخل الهوية الوطنية  ',
    'FORM_GENDER_PLACEHOLDER' =>'أختر جنس',

    'CREAT_BUTTON' => 'تقديم البيانات',
    'CREAT_CANCEL' => 'إلغاء',

];
