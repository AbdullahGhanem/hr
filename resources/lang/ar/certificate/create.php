<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' =>'صفحة إضافة شهادة',
    'SUB_TITLE' => 'إضافة شهادة',
    'MINI'=>'سيتم إضافة شهادة جديدة لنظام',

    'FORM_EMPLOYEE_NAME' =>'اسم الموظف',
    'FORM_STARTDATE' =>'التاريخ ',
    'FORM_NAME_ENGLISH' =>'الاسم الانجليزي',
    'FORM_NAME_ARABIC' =>'الاسم العربي ',
    'FORM_LOCATION' =>'الموقع',
    
    'FORM_LOCATION_PLACEHOLDER' =>'أدخل الموقع',
    'FORM_NAME_AR_PLACEHOLDER' =>'أدخل الاسم باللغة العربية',
    'FORM_NAME_EN_PLACEHOLDER' =>'أدخل الاسم باللغة الإنجليزية',

    'CREAT_BUTTON' => 'تقديم البيانات',
    'CREAT_CANCEL' => 'إلغاء',

];
