<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' =>'صفحة تحديث التجنيد العسكري',
    'SUB_TITLE' => 'تحديث التجنيد العسكري',

    'FORM_EMPLOYEE_NAME' =>'اسم الموظف',
    'FORM_START_DATE'=>'تاريخ البدء',
    'FORM_END_DATE'=>'تاريخ الانتهاء',
    'FORM_Service_Availability'=>' توفر الخدمة',
    'FORM_completed'=>'تم الانتهاء',

    'CREAT_BUTTON' => 'تقديم البيانات',
    'CREAT_CANCEL' => 'إلغاء',

];
