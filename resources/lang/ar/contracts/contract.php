<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => ' صفحة العقود',
    'CONTRACT_TABLE' => 'جدول العقود',
    'SUB_TITLE_CONTRACT_TABLE' => 'جميع العقود في النظام.',


    'CREAT_BUTTON' => 'إنشاء عقد',



    // 'SEARCH_EMPLOYEE_NAME' => 'اسم الموظف',
    // 'SEARCH_EMPLOYEE_EMAIL' => 'البريد الإلكتروني للموظف',
    // 'SEARCH_EMPLOYEE_NATIONAL_ID' => 'الهوية الوطنية للموظف',
    // 'SEARCH_SUPER_NAME' => 'اسم المشرف',
    // 'SEARCH_DEPT_NAME' => 'اسم القسم',
    // 'SEARCH_JOB_NAME' => 'اسم الوظيفة',
    'SEARCH_FOR_NAME' => 'البحث عن اسم الموظف',
    'SEARCH_FOR_NAME' => 'البحث عن طريق الإسم',
    'SEARCH_SEARCH_BTN' => 'بحث',
    'SEARCH_CLEAR_SEARCH_BTN' => 'مسح البحث',

    'TABLE_FILTER' => ' تصفية حسب التاريخ',

    
    'TABLE_HEAD_Employee' => 'موظف',
    'TABLE_HEAD_START_Date' => 'تاريخ البدء',
    'TABLE_HEAD_END_Date' => 'تاريخ الانتهاء',
    'TABLE_HEAD_Active' => 'نشط',
    'TABLE_HEAD_OPTIONS' => 'خيارات',



    'TABLE_JOBTITLE' => 'المسمى الوظيفي',
    'TABLE_Email' => 'البريد الإلكتروني',

];
