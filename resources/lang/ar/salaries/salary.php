<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'صفحة الرواتب',

    'SALARY_TABLE' => 'جدول الرواتب',



    // 'SEARCH_EMPLOYEE_NAME' => 'اسم الموظف',
    // 'SEARCH_EMPLOYEE_EMAIL' => 'البريد الإلكتروني للموظف',
    // 'SEARCH_EMPLOYEE_NATIONAL_ID' => 'الهوية الوطنية للموظف',
    // 'SEARCH_SUPER_NAME' => 'اسم المشرف',
    'SEARCH_DEPT_NAME' => 'اسم القسم',
    // 'SEARCH_JOB_NAME' => 'اسم الوظيفة',
    'SEARCH_FOR_NAME' => 'البحث عن اسم الموظف',
    'SEARCH_SEARCH_BTN' => 'بحث',
    'SEARCH_CLEAR_SEARCH_BTN' => 'مسح البحث',


    

    'TABLE_HEAD_Employee' => 'موظف',
    'TABLE_HEAD_Date' => 'تاريخ',
    'TABLE_HEAD_Contract' => 'عقد',
    'TABLE_HEAD_Total' => 'مجموع',
    'TABLE_HEAD_OPTIONS' => 'خيارات',

    'TABLE_FILTER' => ' تصفية حسب التاريخ',
    'TABLE_ISACTIVE' => ' نشط',
    'TABLE_JOBTITLE' => 'المسمى الوظيفي',
    'TABLE_Email' => 'البريد الإلكتروني',
    'TABLE_EMPLOYEES' => 'موظف',

];
