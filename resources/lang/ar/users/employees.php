<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'صفحة الموظفين',
    'USERS_TABLE' => 'قائمة الموظفين',
    'SUB_TITLE_USERS_TABLE' => 'جميع الموظفين في النظام.',

    'ADD_EMPLOYEE_BUTTON' => 'إضافة موظف جديد',


    'SEARCH_EMPLOYEE_NAME' => 'اسم الموظف',
    'SEARCH_EMPLOYEE_EMAIL' => 'البريد الإلكتروني للموظف',
    'SEARCH_EMPLOYEE_NATIONAL_ID' => 'الهوية الوطنية للموظف',
    'SEARCH_SUPER_NAME' => 'اسم المشرف',
    'SEARCH_DEPT_NAME' => 'اسم القسم',
    'SEARCH_JOB_NAME' => 'اسم الوظيفة',
    'SEARCH_SEARCH_BTN' => 'بحث',
    'SEARCH_CLEAR_SEARCH_BTN' => 'مسح البحث',


    'TABLE_HEAD_NAME' => 'اسم',
    'TABLE_HEAD_SUPER' => 'مشرف',
    'TABLE_HEAD_DEPT' => 'إدارة',
    'TABLE_HEAD_OPTIONS' => 'خيارات',

    'TABLE_JOBTITLE' => 'المسمى الوظيفي',
    'TABLE_Email' => 'البريد الإلكتروني',

];
