<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'إنشاء صفحة موظف',
    'JOB_TABLE' => 'إنشاء موظف',
    'SUB_TITLE_JOB_TABLE' => 'سيتم إضافة موظف جديد إلى النظام.',

    'FORM_NAME_EN' =>'الاسم باللغة العربية',
    'FORM_NAME_AR' =>'الاسم باللغة الانجليزية',
    'FORM_Description_EN' =>'الوصف باللغة الإنجليزية ',
    'FORM_Description_AR' =>'الوصف باللغة العربية',
    'FORM_EMAIL_ADDRESS' =>'عنوان البريد الإلكتروني',
    'FORM_MAIL_ADDRESS' =>'عنوان البريد',
    'FORM_VACATION_DAYS' =>'أيام العطلة',
    'FORM_CITY' =>'بلد',
    'FORM_NATIONAL_ID' =>'الهوية الوطنية ',
    'FORM_TYPE' =>'نوع الموظف',
    'FORM_GENDER' =>'جنس الموظف',
    'FORM_MOBILE' =>'رقم الهاتف المحمول',
    'FORM_JOB' =>'المسمى الوظيفي',
    'FORM_SOCIAL_INSURANCE_NUMBER' =>'رقم التأمين الاجتماعي',
    'FORM_DATE_OF_BIRTH' =>'تاريخ الولادة',
    'FORM_YEARS_OF_EXPERIENCE' =>'سنوات الخبرة',
    'FORM_SUPERVISOR' =>'المشرف',
    'FORM_Start_Hiring_Date' =>'بدء تاريخ التوظيف',
    'FORM_breadwinner' =>'معيل',
    'FORM_rank' =>'رتبته',

    'FORM_rank1' =>'رئيس الفريق ',
    'FORM_rank2' =>'كبير',
    'FORM_rank3' =>'وسط',
    'FORM_rank4' =>'الأصغر',



    'FORM_GENDER_MALE'=>'ذكر',
    'FORM_GENDER_FEMALE'=>'أنثى',

    'FORM_NAME_EN_PLACEHOLDER' =>'أدخل الاسم باللغة الإنجليزية',
    'FORM_DESC_EN_PLACEHOLDER' =>'أدخل الوصف باللغة الإنجليزية',
    'FORM_NAME_AR_PLACEHOLDER' =>'أدخل الاسم باللغة العربية',
    'FORM_DESC_AR_PLACEHOLDER' =>'أدخل الوصف باللغة العربية',
    'FORM_EMAIL_ADDRESS_PLACEHOLDER' =>'أدخل عنوان البريد الالكتروني',
    'FORM_MAIL_ADDRESS_PLACEHOLDER' =>'أدخل عنوان البريد',
    'FORM_VACATION_DAYS_PLACEHOLDER' =>'أدخل أيام العطلة ',
    'FORM_CITY_PLACEHOLDER' =>'أدخل بلد ',
    'FORM_NATIONAL_ID_PLACEHOLDER' =>'أدخل الهوية الوطنية  ',
    'FORM_TYPE_PLACEHOLDER' =>'أختر نوع',
    'FORM_GENDER_PLACEHOLDER' =>'أختر جنس',
    'FORM_MOBILE_PLACEHOLDER' =>'أدخل رقم الهاتف المحمول',
    'FORM_JOB_PLACEHOLDER' =>'أختر المسمى الوظيفي',
    'FORM_SOCIAL_INSURANCE_NUMBER_PLACEHOLDER' =>'أدخل رقم التأمين الاجتماعي',
    'FORM_YEARS_OF_EXPERIENCE_PLACEHOLDER' =>'أدخل سنوات الخبرة',
    'FORM_SUPERVISOR_PLACEHOLDER' =>'أختر المشرف ',
    'FORM_rank_PLACEHOLDER' =>'أختر رتبته ',


    'CREAT_BUTTON' => 'تقديم البيانات',
    'CREAT_CANCEL' => 'إلغاء',

];
