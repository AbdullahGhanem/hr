<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'صفحة تغيير كلمة المرور',
    'SUB_TITLE' => 'تغيير كلمة المرور',

    'FORM_OLD' => ' كلمة السر القديمة',
    'FORM_NEW' => ' كلمة السر الجديدة',
    'FORM_CONFIRM' => ' تأكيد كلمة المرور الجديدة',

    'CREAT_BUTTON' => 'تقديم البيانات',
    'CREAT_CANCEL' => 'إلغاء',
 

];
