<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'إنشاء صفحة عنوان الوظيفة',
    'JOB_TABLE' => 'إنشاء عنوان الوظيفة',
    'SUB_TITLE_JOB_TABLE' => 'سيتم إضافة عنوان وظيفي جديد إلى النظام.',

    'FORM_NAME_EN' =>'الاسم باللغة العربية',
    'FORM_NAME_AR' =>'الاسم باللغة الانجليزية',
    'FORM_Description_EN' =>'الوصف باللغة الإنجليزية ',
    'FORM_Description_AR' =>'الوصف باللغة العربية',



    'FORM_NAME_EN_PLACEHOLDER' =>'أدخل الاسم باللغة الإنجليزية',
    'FORM_DESC_EN_PLACEHOLDER' =>'أدخل الوصف باللغة الإنجليزية',
    'FORM_NAME_AR_PLACEHOLDER' =>'أدخل الاسم باللغة العربية',
    'FORM_DESC_AR_PLACEHOLDER' =>'أدخل الوصف باللغة العربية',

    'CREAT_BUTTON' => 'تقديم البيانات',
    'CREAT_CANCEL' => 'إلغاء',

];
