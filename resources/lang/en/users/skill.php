<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Create User Skill Page',
    'SUB_TITLE' => 'Create User Skill',


    'FORM_NAME' =>'EMPLOYEE NAME',
    'FORM_SKILL' =>'SKILL',
    'FORM_LEVEL' =>'LEVEL',
    'FORM_DATE' =>'DATE ACQUIRED',
    
    'FORM_DEPT_PLACEHOLDER' =>'Choose Skill',
    'FORM_level_PLACEHOLDER' =>'Choose lEVEL',


    'CREAT_BUTTON' => 'Submit Data',
    'CREAT_CANCEL' => 'Cancel',
    
];
