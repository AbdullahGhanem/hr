<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Super Admins Page',
    'USERS_TABLE' => 'Super Admins TABLE',
    'SUB_TITLE_USERS_TABLE' => 'All Super Admins in System.',


    'SEARCH_EMPLOYEE_NAME' => 'Employee name',
    'SEARCH_EMPLOYEE_EMAIL' => 'Employee email',
    'SEARCH_EMPLOYEE_NATIONAL_ID' => 'Employee national id',
    'SEARCH_SUPER_NAME' => 'Supervisor name',
    'SEARCH_DEPT_NAME' => 'Department name',
    'SEARCH_JOB_NAME' => 'Job title name',

    'SEARCH_SEARCH_BTN' => 'Search',
    'SEARCH_CLEAR_SEARCH_BTN' => 'Clear Search',
    
    
    
    'TABLE_HEAD_NAME' => 'Name',
    'TABLE_HEAD_EMAIL' => 'Email',
    'TABLE_HEAD_RANK' => 'Rank',
    'TABLE_HEAD_DEPT' => 'Department',
    'TABLE_HEAD_OPTIONS' => 'Options',

    'TABLE_JOBTITLE' => 'Job Title',
    'TABLE_Email' => 'Email',
    
];
