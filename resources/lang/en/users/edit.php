<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Edit Employee Data Page',
    'JOB_TABLE' => 'Edit Employee Data',

    'CREAT_BUTTON' => 'Create Employee',

    'FORM_NAME_EN' =>'Name in english ',
    'FORM_NAME_AR' =>'Name in arabic',
    // 'FORM_Description_EN' =>'Description in english ',
    // 'FORM_Description_AR' =>'Description in arabic',
    'FORM_EMAIL_ADDRESS' =>'Email Address',
    'FORM_MAIL_ADDRESS' =>'Mail Address',
    'FORM_VACATION_DAYS' =>'Vacation Days',
    'FORM_country' =>'Country',
    'FORM_NATIONAL_ID' =>'National ID ',
    'FORM_TYPE' =>'Type',
    'FORM_GENDER' =>'Gender',
    'FORM_MOBILE' =>'Mobile Number',
    'FORM_JOB' =>'JOB TITLE ',
    'FORM_SOCIAL_INSURANCE_NUMBER' =>'SOCIAL INSURANCE NUMBER',
    'FORM_DATE_OF_BIRTH' =>'DATE OF BIRTH',
    'FORM_YEARS_OF_EXPERIENCE' =>'YEARS OF EXPERIENCE',
    'FORM_SUPERVISOR' =>'SUPERVISOR',
    'FORM_Start_Hiring_Date' =>'Start Hiring Date',
    'FORM_rank' =>'Rank',
    'FORM_breadwinner' =>'is the breadwinner',

    'FORM_rank1' =>'Team Leader',
    'FORM_rank2' =>'Senior',
    'FORM_rank3' =>'Middle',
    'FORM_rank4' =>'Junior',
    
    'FORM_GENDER_MALE'=>'Male',
    'FORM_GENDER_FEMALE'=>'Female',

    'FORM_NAME_EN_PLACEHOLDER' =>'Enter name in english ',
    'FORM_DESC_EN_PLACEHOLDER' =>'Enter description in english ',
    'FORM_NAME_AR_PLACEHOLDER' =>'Enter name in arabic ',
    'FORM_DESC_AR_PLACEHOLDER' =>'Enter description in arabic ',
    'FORM_EMAIL_ADDRESS_PLACEHOLDER' =>'Enter Email Address ',
    'FORM_MAIL_ADDRESS_PLACEHOLDER' =>'Enter Mail Address ',
    'FORM_VACATION_DAYS_PLACEHOLDER' =>'Enter Vacation Days ',
    'FORM_CITY_PLACEHOLDER' =>'Choose Country ',
    'FORM_NATIONAL_ID_PLACEHOLDER' =>'Enter National ID  ',
    'FORM_TYPE_PLACEHOLDER' =>'Choose Type',
    'FORM_GENDER_PLACEHOLDER' =>'Choose Gender',
    'FORM_MOBILE_PLACEHOLDER' =>'Enter Mobile Number',
    'FORM_JOB_PLACEHOLDER' =>'Choose job title',
    'FORM_SOCIAL_INSURANCE_NUMBER_PLACEHOLDER' =>'Enter social insurance number ',
    'FORM_YEARS_OF_EXPERIENCE_PLACEHOLDER' =>'Enter years of experiance  ',
    'FORM_SUPERVISOR_PLACEHOLDER' =>'Choose supervisor ',
    'FORM_SUPERVISOR_PLACEHOLDER' =>'Choose supervisor ',
    'FORM_rank_PLACEHOLDER' =>'Choose rank ',
    
    

    'CREAT_BUTTON' => 'Submit Data',
    'CREAT_CANCEL' => 'Cancel',
    
];
