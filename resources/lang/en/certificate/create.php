<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Add Certificate Page',
    'SUB_TITLE' => 'Add Certificate',
    'MINI'=>'New Certificate will be added to system',

    'FORM_EMPLOYEE_NAME' =>'EMPLOYEE NAME',
    'FORM_DATE' =>'DATE',
    'FORM_LOCATION' =>'LOCATION',
    'FORM_NAME_ENGLISH' =>'ENGLISH NAME',
    'FORM_NAME_ARABIC' =>'ARABIC NAME',
    
    'FORM_NAME_AR_PLACEHOLDER' =>'ENTER NAME IN ARABIC',
    'FORM_NAME_EN_PLACEHOLDER' =>'ENTER NAME IN ENGLISH',
    'FORM_LOCATION_PLACEHOLDER' =>'ENTER LOCATION',

    'CREAT_BUTTON' => 'Submit Data',
    'CREAT_CANCEL' => 'Cancel',
    
];
