<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Add Family Member Page',
    'SUB_TITLE' => 'Add Family Member',

    'FORM_EMPLOYEE_NAME' =>'EMPLOYEE NAME',
    'FORM_dob'=>' DATE OF BIRTH',
    'FORM_NAME_EN' =>'Name in english ',
    'FORM_NAME_AR' =>'Name in arabic',
    'FORM_email'=>' EMAIL',
    'FORM_NATIONAL_ID'=>' NATIONAL ID ',
    'FORM_GENDER'=>' GENDER',

    'FORM_GENDER_MALE'=>'Male',
    'FORM_GENDER_FEMALE'=>'Female',

    'FORM_NAME_EN_PLACEHOLDER' =>'Enter name in english ',
    'FORM_NAME_AR_PLACEHOLDER' =>'Enter name in arabic ',
    'FORM_EMAIL_ADDRESS_PLACEHOLDER' =>'Enter Email Address ',
    'FORM_NATIONAL_ID_PLACEHOLDER' =>'Enter National ID  ',
    'FORM_GENDER_PLACEHOLDER' =>'Choose Gender',

    'CREAT_BUTTON' => 'Submit Data',
    'CREAT_CANCEL' => 'Cancel',
    
];
