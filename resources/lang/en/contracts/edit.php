<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Edit Contract Page',
    'CONTRACT_TABLE' => 'Edit Contract',

    'CREAT_BUTTON' => 'Edit Contract',
    'FORM_USER' =>'User',
    'FORM_MONTHLY_SALARY' =>'Monthly Salary',
    'FORM_CURRANCY' =>'Currancy',
    'FORM_ACTIVE' =>'Active',
    'FORM_START_DATE' =>'Start Date',
    'FORM_END_DATE' =>'End Date',

    'FORM_MONTHLY_SALARY_PLACEHOLDER' =>'Enter Monthly Salary',
    'FORM_CURRANCY_PLACEHOLDER' =>'Select Currancy',

    'CREAT_BUTTON' => 'Submit Data',
    'CREAT_CANCEL' => 'Cancel',
    
];
