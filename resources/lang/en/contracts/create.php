<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Create Contract Page',
    'CONTRACT_TABLE' => 'Create Contract',
    'SUB_TITLE_CONTRACT_TABLE' => 'New contract will be added to system.',

    'CREAT_BUTTON' => 'Create Contract',
    'FORM_USER' =>'User',
    'FORM_MONTHLY_SALARY' =>'Monthly Salary',
    'FORM_CURRANCY' =>'Currancy',
    'FORM_ACTIVE' =>'Active',
    'FORM_START_DATE' =>'Start Date',
    'FORM_END_DATE' =>'End Date',

    'FORM_MONTHLY_SALARY_PLACEHOLDER' =>'Enter Monthly Salary',
    'FORM_CURRANCY_PLACEHOLDER' =>'Select Currancy',

    'CREAT_CANCEL' => 'Cancel',
    'CREAT_BUTTON' => 'Submit Data',
    
];
