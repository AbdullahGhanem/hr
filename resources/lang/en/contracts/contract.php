<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Contracts Page',
    'CONTRACT_TABLE' => 'Contracts TABLE',
    'SUB_TITLE_CONTRACT_TABLE' => 'All Contracts in System.',

    'CREAT_BUTTON' => 'Create Contract',



    // 'SEARCH_EMPLOYEE_NAME' => 'Employee name',
    // 'SEARCH_EMPLOYEE_EMAIL' => 'Employee email',
    // 'SEARCH_EMPLOYEE_NATIONAL_ID' => 'Employee national id',
    // 'SEARCH_SUPER_NAME' => 'Supervisor name',
    // 'SEARCH_DEPT_NAME' => 'Department name',
    // 'SEARCH_JOB_NAME' => 'Job title name',
    'SEARCH_FOR_NAME' => 'Search For Employee Name',
    'SEARCH_FOR_NAME' => 'Search By Name',
    'SEARCH_SEARCH_BTN' => 'Search',
    'SEARCH_CLEAR_SEARCH_BTN' => 'Clear Search',
    
    
    'TABLE_FILTER' => ' Filter By Date',

   
    'TABLE_HEAD_Employee' => 'Employee',
    'TABLE_HEAD_START_Date' => 'Start Date',
    'TABLE_HEAD_END_Date' => 'End Date',
    'TABLE_HEAD_Active' => 'Active',
    'TABLE_HEAD_OPTIONS' => 'Options',

    'TABLE_JOBTITLE' => 'Job Title',
    'TABLE_Email' => 'Email',
    
];
