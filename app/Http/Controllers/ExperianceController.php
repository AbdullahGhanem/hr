<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Experience;
use App\Http\Requests\ExperienceRequest;
use App\Http\Requests\ExperienceUpdateRequest;

class ExperianceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $employee = User::find($id);
        if (!$employee) {
            $this->flash('employee can not be found' , 'danger');
            return redirect()->back();
        }
        return view('experiance.create',compact('employee'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ExperienceRequest $request)
    {
        $experience = Experience::create([
            'user_id' => $request->get('user_id'),
            'start_date' =>  $request->get('start_date'),
            'end_date' => $request->get('end_date'),
            'where' =>  $request->get('where'),
            'role_en' => $request->get('role_en'),
            'role_ar' => $request->get('role_ar'),
            'description_en' => $request->get('description_en'),
            'description_ar' => $request->get('description_ar'),
        ]);

        if(!$experience){
            $this->flash('user experience can not be added' , 'danger');
        }else{
            $this->flash('user experience added successfully' , 'success');
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $experience = Experience::with('user')->where('id',$id)->first();
        if (!$experience) {
            $this->flash('experience can not be found' , 'danger');
            return redirect()->back();
        }
        return view('experiance.edit',compact('experience'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ExperienceUpdateRequest $request, $id)
    {
        $experience = Experience::find($id);
        if (!$experience) {
            $this->flash('experience can not be found' , 'danger');
            return redirect()->back();
        }

        $updated = $experience->update([
            'start_date' =>  $request->get('start_date'),
            'end_date' => $request->get('end_date'),
            'where' =>  $request->get('where'),
            'role_en' => $request->get('role_en'),
            'role_ar' => $request->get('role_ar'),
            'description_en' => $request->get('description_en'),
            'description_ar' => $request->get('description_ar'),
        ]);

        if(!$updated){
            $this->flash('user experience can not be updated' , 'danger');
        }else{
            $this->flash('user experience updated successfully' , 'success');
        }
        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $experience = Experience::find($id);
        if (!$experience) {
            $this->flash('experience can not be found' , 'danger');
        }
        if ($experience->delete()) {
            $this->flash('experience deleted successfully' , 'success');
        }else{
            $this->flash('experience can not be deleted' , 'danger');
        }
        return redirect()->back();
    }
}
