<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contract;
use App\User;
use App\Http\Requests\ContractRequest;
use App\Http\Requests\ContractUpdateRequest;
use App\Events\NewContract;

class ContractController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $archives = Contract::Schedule();
        $contracts = Contract::with('salaries','user','user.type','user.jobTitle','user.jobTitle.department')
                            ->orderBy('start_date','desc')
                            ->filter( request('month'), request('year') )
                            ->filterEmployee(  request('emp_name') )
                            ->paginate(10);

        if (request('month') && request('year')) {
            $date = request('month') .' '.request('year');   
        }else{
            $date = 'All';   
        }
        return view('contract.all',compact('contracts','archives','date'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        return view('contract.create',compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContractRequest $request)
    {
        //dd($request->all());
        $user = User::with(['contracts'])->where('id',$request->get('user_id'))->first();
        if (!$user) {
            $this->flash('User can not be found' , 'danger');
            return redirect()->back();
        }
        if ($request->has('is_active') && $request->get('is_active') == "on" ) {
            $activeContract = Contract::contractActive($request->get('user_id'));
            if ($activeContract->first()) {
                $this->flash('User have already active contract' , 'danger');
                return redirect()->back();
            }
        }
        $existContract = Contract::contractExist(
            $request->get('start_date'),
            $request->get('end_date'),
            $request->get('user_id')
        );
        if ($existContract->first()) {
            $this->flash('Contract is exist already' , 'danger');
            return redirect()->back();
        }

        $new_contract = Contract::create([
            'user_id' => $request->get('user_id'),
            'start_date' => $request->get('start_date'),
            'end_date' => $request->get('end_date'),
            'is_active' => ($request->has('is_active') && $request->get('is_active') == "on"  ) ? true : false ,
            'monthly_salary' => $request->get('monthly_salary'),
            'currancy' => $request->get('currancy'),
            'total_annual' => ( $request->get('monthly_salary') * 12 ),
        ]);

        if (!$new_contract) {
            $this->flash(' contract' , 'danger','contract can not be added');
        }else{
            //send notification to user by new contract
            event(new NewContract($user , $new_contract ));
            $this->flash('contract' , 'success','contract added successfully');
        }
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contract = Contract::with('user')->where('id',$id)->first();
        if (!$contract) {
            $this->flash('contract can not be found' , 'danger');
            return redirect()->back();
        }
        return view('contract.edit',compact('contract'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ContractUpdateRequest $request, $id)
    {
        $contract = Contract::find($id);
        if (!$contract) {
            $this->flash('contract can not be found' , 'danger');
            return redirect()->back();
        }

        if ($request->has('is_active') && $request->get('is_active') == "on" ) {
            $activeContract = Contract::contractActive($request->get('user_id'));
            if ($activeContract->first() ) {
                if($activeContract->first()->id != $id){
                    $this->flash('User have already active contract' , 'danger');
                    return redirect()->back();
                }
            }
        }

        $new_contract = $contract->update([
            'start_date' => $request->get('start_date'),
            'end_date' => $request->get('end_date'),
            'is_active' => ($request->has('is_active') && $request->get('is_active') == "on"  ) ? true : false ,
            'monthly_salary' => $request->get('monthly_salary'),
            'currancy' => $request->get('currancy'),
            'total_annual' => ( $request->get('monthly_salary') * 12 ),
        ]);

        if (!$new_contract) {
            $this->flash(' contract' , 'danger','contract can not be updated');
        }else{
            $this->flash('contract' , 'success','contract updated successfully');
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contract = Contract::find($id);
        if (!$contract) {
            $this->flash('Contract can not be found' , 'danger');
        }
        if ($contract->delete()) {
            if (count($contract->salaries))
                $contract->salaries->each->delete();
            $this->flash('Contract deleted successfully' , 'success');
        }else{
            $this->flash('Contract can not be deleted' , 'danger');
        }
        return redirect()->back();
    }
}
