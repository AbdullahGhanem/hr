<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use App\Http\Requests\DepartmentRequest;
use App\Http\Requests\DepartmentUpdateRequest;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::with(['jobTitles'])
                                    ->filterName( request('name') )
                                    ->paginate(10);
        return view('department.all',compact('departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('department.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\DepartmentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DepartmentRequest $request)
    {
        $department = Department::create([
            'name_en'       => $request->get('name_en'),
            'name_ar'       => $request->get('name_ar'),
            'description_en'=> $request->get('description_en'),
            'description_ar'=> $request->get('description_ar'),
        ]);
        if(!$department){
            $this->flash('department can not be added' , 'danger');
        }else{
            $this->flash('department added successfully' , 'success');
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $department = Department::find($id);
        if (!$department) {
            $this->flash('department can not be found' , 'danger');
            return redirect()->back();
        }
        return view('department.edit',compact('department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\DepartmentUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DepartmentUpdateRequest $request, $id)
    {
        $department = Department::find($id);
        if (!$department) {
            $this->flash('department can not be found' , 'danger');
            return redirect()->back();
        }
        $updated = $department->update([
            'name_en'           => $request->get('name_en'), 
            'name_ar'           => $request->get('name_ar'), 
            'description_en'    => $request->get('description_en'), 
            'description_ar'    => $request->get('description_ar'), 
        ]);

        if (!$updated) {
            $this->flash('Department can not be updated' , 'danger');
        }else{
            $this->flash('Department updated successfully' , 'success');
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $department = Department::find($id);
        if (!$department) {
            $this->flash('department can not be found' , 'danger');
        }
        if ($department->delete()) {
            $this->flash('department deleted successfully' , 'success');
        }else{
            $this->flash('department can not be deleted' , 'danger');
        }
        return redirect()->back();
    }
}
