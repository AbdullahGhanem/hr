<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Recruitment;
use App\Http\Requests\RecruitmentRequest;
use App\Http\Requests\RecruitmentUpdateRequest;

class RecruitmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $employee = User::find($id);
        if (!$employee) {
            $this->flash('employee can not be found' , 'danger');
            return redirect()->back();
        }
        return view('recruitment.create',compact('employee'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RecruitmentRequest $request)
    {
        $recruitment_exist = Recruitment::recruitmentExist($request->get('user_id'))->first();
        if ($recruitment_exist) {
            $this->flash('user recruitment is already exist' , 'danger');
            return redirect()->back();
        }
        $recruitment = Recruitment::create([
            'user_id'=>$request->get('user_id'),
            'start_date'=>$request->get('start_date'),
            'end_date'=>$request->get('end_date'),
            'service_availability'=> ( $request->has('availability') ) ? true : false ,
            'completed'=> ( $request->has('completed') ) ? true : false
        ]);

        if(!$recruitment){
            $this->flash('user recruitment can not be added' , 'danger');
        }else{
            $this->flash('user recruitment added successfully' , 'success');
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $recruitment = Recruitment::with('user')->where('id',$id)->first();
        if (!$recruitment) {
            $this->flash('recruitment can not be found' , 'danger');
            return redirect()->back();
        }
        return view('recruitment.edit',compact('recruitment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RecruitmentUpdateRequest $request, $id)
    {
        $recruitment = Recruitment::find($id);
        if (!$recruitment) {
            $this->flash('Recruitment can not be found' , 'danger');
            return redirect()->back();
        }

        $updated = $recruitment->update([
            'start_date'=>$request->get('start_date'),
            'end_date'=>$request->get('end_date'),
            'service_availability'=> ( $request->has('availability') ) ? true : false ,
            'completed'=> ( $request->has('completed') ) ? true : false
        ]);

        if(!$updated){
            $this->flash('user Recruitment can not be updated' , 'danger');
        }else{
            $this->flash('user Recruitment updated successfully' , 'success');
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
