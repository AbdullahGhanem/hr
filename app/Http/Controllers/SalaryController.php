<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Salary;
use App\User;
use App\Contract;
use Carbon\Carbon;

use App\Http\Requests\SalaryRequest;
use App\Http\Requests\SalaryUpdateRequest;
use App\Repositories\Backend\SalaryRepository;

use  App\Events\NewSalary;

class SalaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $archives = Salary::Schedule();
        $salaries = Salary::with('contract','contract.user','contract.user.type','contract.user.jobTitle','contract.user.jobTitle.department')
                            ->orderBy('date','desc')
                            ->filter( request('month'), request('year') )
                            ->filterEmployee( request('emp_name') )
                            ->paginate(10);

        if (request('month') && request('year')) {
            $date = request('month') .' '.request('year');   
        }else{
            $date = 'All';   
        }
        return view('salary.all',compact('salaries','archives','date'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($employee_id)
    {
        $employee = User::find($employee_id);
        if (!$employee) {
            $this->flash('Employee can not be found' , 'danger');
            return redirect()->back();
        }

        $active_contract = Contract::contractActive($employee->id)->first();
        if (!$active_contract) {
            $this->flash('No active contract' , 'danger');
            return redirect()->back();
        }

        $today = Carbon::now()->format('Y-m'); // month and year
        return view('salary.create',compact('employee','active_contract','today'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SalaryRequest $request , SalaryRepository $repository)
    {
        $date = Carbon::parse($request->get('date'))->format('Y-m-1'); // parse month to date
        $find = Salary::salaryExist($date,$request->get('contract_id'));
		if ($find->first()) {
            $this->flash('salary already exist' , 'danger');
            return redirect()->back();
        }

        $salaryCalc = $repository->calculateSalary($request); // calculate salary

        $salary = Salary::create([
            'contract_id' => $request->get('contract_id'),
            'date' => $date,
            'incentive' => $request->get('incentive'),
            'deduction' => $request->get('deduction'),
            'other' => $request->get('other'),
            'allowance' => $request->get('allowance'),
            'tax' => $request->get('tax'),
            'loan' => $request->get('loan'),
            'net' => $salaryCalc['net_salary'] ,
            'total' => $salaryCalc['total'],
       ]);
       if(!$salary){
            $this->flash('salary can not be added' , 'danger');
        }else{
            $contract = Contract::where('id',$request->get('contract_id'))->first();
            if (!$contract) {
                \Log::info(['create new salary contract not found by id ' =>$request->get('contract_id')] );
            }
            $user = $contract->user_id;
            $user = User::find($user);
            if (!$user) {
                $this->flash('user not found' , 'danger');
                return redirect()->back();
            }
            //send notification to user by new salary
            event(new NewSalary($user , $salary ));
            $this->flash('salary added successfully' , 'success');
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $salary = Salary::with(['contract','contract.user'])->where('id',$id)->first();
        if (!$salary) {
            $this->flash('salary can not be found' , 'danger');
            return redirect()->back();
        }
        return view('salary.edit',compact('salary'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\SalaryUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SalaryUpdateRequest $request, $id , SalaryRepository $repository)
    {
        $salary = Salary::find($id);
        if (!$salary) {
            $this->flash('salary can not be found' , 'danger');
            return redirect()->back();
        }

        $salaryCalc = $repository->calculateSalary($request);

        $updated = $salary->update([
            'incentive' => $request->get('incentive'),
            'deduction' => $request->get('deduction'),
            'other' => $request->get('other'),
            'allowance' => $request->get('allowance'),
            'tax' => $request->get('tax'),
            'loan' => $request->get('loan'),
            'net' => $salaryCalc['net_salary'] ,
            'total' => $salaryCalc['total'],
        ]);
        if (!$updated) {
            $this->flash('salary can not be updated' , 'danger');
           
        }else{
            $this->flash('salary updated successfully' , 'success');
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $salary = Salary::find($id);
        if (!$salary) {
            $this->flash('salary can not be found' , 'danger');
        }
        if ($salary->delete()) {
            $this->flash('salary deleted successfully' , 'success');
        }else{
            $this->flash('salary can not be deleted' , 'danger');
        }
        return redirect()->back();
    }
}
