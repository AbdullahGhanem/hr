<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\FamilyMember;
use App\SocialStatus;
use App\Http\Requests\FamilyMemberRequest;
use App\Http\Requests\FamilyMemberUpdateRequest;

class FamilyMembersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $employee = User::find($id);
        if (!$employee) {
            $this->flash('Employee can not be found' , 'danger');
            return redirect()->back();
        }
       
        return view('familyMember.create',compact('employee'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FamilyMemberRequest $request)
    {
        $socialStatus = SocialStatus::where('user_id',$request->get('user_id'))->first();
		if (!$socialStatus) {
            $this->flash('Social Status not found' , 'danger');
            return redirect()->back();
        }

        $familyMember = FamilyMember::create([
            'social_status_id' => $socialStatus->id,
            'date' => $request->get('date') ,
            'name_en' => $request->get('name_en') ,
            'name_ar' => $request->get('name_ar') ,
            'email' => $request->get('email') ,
            'national_id' => $request->get('national_id') ,
            'gender' => $request->get('gender') ,
        ]);

        if(!$familyMember){
            $this->flash('User Family Member can not be added' , 'danger');
        }else{
            $this->flash('User Family Member added successfully' , 'success');
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $familyMember = FamilyMember::with(['socialStatus','socialStatus.user'])->where('id',$id)->first();
        if (!$familyMember) {
            $this->flash('FamilyMember can not be found' , 'danger');
            return redirect()->back();
        }
        return view('familyMember.edit',compact('familyMember'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FamilyMemberUpdateRequest $request, $id)
    {
        $familyMember = FamilyMember::find($id);
        if (!$familyMember) {
            $this->flash('family member can not be found' , 'danger');
            return redirect()->back();
        }
        $updated = $familyMember->update([
            'date' => $request->get('date') ,
            'name_en' => $request->get('name_en') ,
            'name_ar' => $request->get('name_ar') ,
            'email' => $request->get('email') ,
            'national_id' => $request->get('national_id') ,
            'gender' => $request->get('gender') ,
        ]);
        if (!$updated) {
            $this->flash('family member can not be updated' , 'danger');
           
        }else{
            $this->flash('family member updated successfully' , 'success');
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $familyMember = FamilyMember::find($id);
        if (!$familyMember) {
            $this->flash('Family Member can not be found' , 'danger');
        }
        if ($familyMember->delete()) {
            $this->flash('Family Member deleted successfully' , 'success');
        }else{
            $this->flash('Family Member can not be deleted' , 'danger');
        }
        return redirect()->back();
    }
}
