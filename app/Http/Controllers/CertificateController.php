<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Certificate;
use  App\Http\Requests\CertificateRequest;
use  App\Http\Requests\CertificateUpdateRequest;

class CertificateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $employee = User::find($id);
        if (!$employee) {

            if (\App::getLocale() == 'ar') {
                $this->flash('لا يمكن العثور على الموظف' , 'danger');
            }else if(\App::getLocale() == 'en'){
                $this->flash('employee can not be found' , 'danger');
            }

            return redirect()->back();
        }
        return view('certificate.create',compact('employee'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CertificateRequest $request)
    {
        $certificate = Certificate::create([
            'user_id' => $request->get('user_id'),
            'date' => $request->get('date'),
            'name_en' => $request->get('name_en'),
            'name_ar' => $request->get('name_ar'),
            'location' => $request->get('location'),
        ]);

        if(!$certificate){
            if (\App::getLocale() == 'ar') {
                $this->flash('لا يمكن إضافة شهادة للمستخدم' , 'danger');
            }else if(\App::getLocale() == 'en'){
                $this->flash('user certificate can not be added' , 'danger');
            }
        }else{
            if (\App::getLocale() == 'ar') {
                $this->flash('تمت إضافة شهادة للمستخدم بنجاح' , 'success');
            }else if(\App::getLocale() == 'en'){
                $this->flash('user certificate added successfully' , 'success');
            }
        }
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $certificate = Certificate::with('user')->where('id',$id)->first();
        if (!$certificate) {
            if (\App::getLocale() == 'ar') {
                $this->flash('لا يمكن العثور على شهادة' , 'danger');
            }else if(\App::getLocale() == 'en'){
                $this->flash('Certificate can not be found' , 'danger');
            }
            return redirect()->back();
        }
        return view('certificate.edit',compact('certificate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CertificateUpdateRequest $request, $id)
    {
        $certificate = Certificate::find($id);
        if (!$certificate) {
            if (\App::getLocale() == 'ar') {
                $this->flash('لا يمكن العثور على شهادة' , 'danger');
            }else if(\App::getLocale() == 'en'){
                $this->flash('certificate can not be found' , 'danger');
            }
            return redirect()->back();
        }

        $updated = $certificate->update([
            'date' => $request->get('date'),
            'name_en' => $request->get('name_en'),
            'name_ar' => $request->get('name_ar'),
            'location' => $request->get('location'),
        ]);

        if(!$updated){
            if (\App::getLocale() == 'ar') {
                $this->flash('لا يمكن تحديث شهادة المستخدم' , 'danger');
            }else if(\App::getLocale() == 'en'){
                $this->flash('user certificate can not be updated' , 'danger');
            }
        }else{
            if (\App::getLocale() == 'ar') {
                $this->flash('تم تحديث شهادة المستخدم بنجاح' , 'success');
            }else if(\App::getLocale() == 'en'){
                $this->flash('user certificate updated successfully' , 'success');
            }
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $certificate = Certificate::find($id);
        if (!$certificate) {
            if (\App::getLocale() == 'ar') {
                $this->flash('لا يمكن العثور على شهادة' , 'danger');
            }else if(\App::getLocale() == 'en'){
                $this->flash('certificate can not be found' , 'danger');
            }
        }
        if ($certificate->delete()) {
            if (\App::getLocale() == 'ar') {
                $this->flash('حذف الشهادة' , 'success');
            }else if(\App::getLocale() == 'en'){
                $this->flash('certificate deleted successfully' , 'success');
            }
        }else{
            if (\App::getLocale() == 'ar') {
                $this->flash('لا يمكن حذف الشهادة' , 'danger');
            }else if(\App::getLocale() == 'en'){
                $this->flash('certificate can not be deleted' , 'danger');
            }
        }
        return redirect()->back();
    }
}
