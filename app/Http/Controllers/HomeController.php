<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Validator;

use App\Department;
use App\JobTitle;
use App\Contract;

use App\Repositories\Backend\DashboardRepository;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['changeLang']] );
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DashboardRepository $repo )
    {
        $users          = $repo->get_users_count_by_type();        
        $employees      = $repo->get_users_count_by_type(1);
        $admins         = $repo->get_users_count_by_type(2);
        $super_admins   = $repo->get_users_count_by_type(3);
       
        $departments        = Department::count();
        $jobTitles          = JobTitle::count();
        $active_contracts   = Contract::filterActive()->get();

        $top_employees_salaries     =  $repo->get_top_incentive_employees();
        $total_incentives           =  $repo->get_total_salaries_by_column('incentive');
        $total_deductions           =  $repo->get_total_salaries_by_column('deduction');
        $total_salaries             =  $repo->get_total_salaries_by_column();

        return  view('user.dashboard',
                compact('users','employees','admins',
                        'super_admins','departments','jobTitles',
                        'active_contracts','top_employees_salaries',
                        'total_incentives','total_deductions','total_salaries'));
    }




    public function changeLang($locale){
        if ($locale === "en" || $locale === "ar" ) {
            \Session::put('locale', $locale);
        }
        return redirect()->back();
    }
}
