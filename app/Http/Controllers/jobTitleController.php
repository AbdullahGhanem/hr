<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JobTitle;
use App\Department;
use App\Http\Requests\jobTitleRequest;
use App\Http\Requests\JobTitleUpdateRequest;

class jobTitleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobTitles = JobTitle::with(['users','department'])
                                ->filterName( request('name') )
                                ->filterDepartment( request('dept_name') )
                                ->paginate(10);
        return view('jobTitle.all',compact('jobTitles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::all();
        return view('jobTitle.create',compact('departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\jobTitleRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(jobTitleRequest $request)
    {
        $department = Department::find(1);
        if (!$department) {
            $this->flash('department can not be found' , 'danger');
            return redirect()->back();
        }
        $jobTitle = new JobTitle;
        $jobTitle->name_en = $request->get('name_en');
        $jobTitle->name_ar = $request->get('name_ar');

        if(!$department->jobTitles()->save($jobTitle)){
            $this->flash('job title can not be added' , 'danger');
        }else{
            $this->flash('job title added successfully' , 'success');
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jobTitle = JobTitle::find($id);
        if (!$jobTitle) {
            $this->flash('job title can not be found' , 'danger');
            return redirect()->back();
        }
        $departments = Department::all();
        return view('jobTitle.edit',compact('jobTitle','departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\JobTitleUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(JobTitleUpdateRequest $request, $id)
    {
        $jobTitle = JobTitle::find($id);
        if (!$jobTitle) {
            $this->flash('jobTitle can not be found' , 'danger');
            return redirect()->back();
        }
        $updated = $jobTitle->update([
            'name_en'           => $request->get('name_en'), 
            'name_ar'           => $request->get('name_ar'), 
            'department_id'    => $request->get('department_id'), 
        ]);

        if (!$updated) {
            $this->flash('Job Title can not be updated' , 'danger');
        }else{
            $this->flash('Job Title updated successfully' , 'success');
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $JobTitle = JobTitle::find($id);
        if (!$JobTitle) {
            $this->flash('JobTitle can not be found' , 'danger');
        }
        if ($JobTitle->delete()) {
            $this->flash('JobTitle deleted successfully' , 'success');
        }else{
            $this->flash('JobTitle can not be deleted' , 'danger');
        }
        return redirect()->back();
    }
}
