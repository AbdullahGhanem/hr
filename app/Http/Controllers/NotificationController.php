<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class NotificationController extends Controller
{
    public function readAll(){
       $user =  Auth::user()->unreadNotifications->markAsRead();
       return redirect()->back();
    }

    public function read($id){
        $notification = auth()->user()->notifications()
            			    ->where('id',$id)
                            ->first();
        
        if (!$notification->read_at) {
            $notification->markAsRead();
        }
        $model_type = $notification->data['body']['type'];
        $model_id = $notification->data['body']['id'];

        return $this->notificationRedirect($model_type, $model_id);
    }


    private function notificationRedirect($type , $id){

        switch ($type) {
            case 'NewContract':
                return redirect()->route('employee',Auth::user()->id . '?Contract_page');
                break;
            case 'NewSalary':
                return redirect()->route('employee',Auth::user()->id . '?Salarie_page');
                break;
            case 'NewSkill':
                return redirect()->route('employee',Auth::user()->id);
                break;
            default:
                return redirect()->route('home');
                break;
        }
    }
}
