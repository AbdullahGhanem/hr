<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\User;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user_id = User::find($this->id);
        return [
            'name_en'                   => ' required | max:30 | min:2 ' ,
            "name_ar"                   => ' required | max:30 | min:2 ' ,
            "email"                     => ' required | unique:users,email,'.$user_id->id  ,
            "address"                   => ' required ' ,
            "total_vacation_days"       => ' required | numeric' ,
            "country"                      => ' required | numeric' ,
            "national_id"               => ' required | numeric | unique:users,national_id,'.$user_id->id ,
            "employee_type"             => ' required | numeric' ,
            "gender"                    => ' required' ,
            "mobile_number"             => ' required | unique:users,mobile_number,'.$user_id->id ,
            "job_title_id"              => ' required | numeric' ,
            "social_insurance_Number"   => ' required | numeric | unique:users,social_insurance_Number,'.$user_id->id ,
            "image"                     => ' nullable ' ,
            "dob"                       => ' required ' ,
            "years_of_experience"       => ' required | numeric' ,
            "supervisor_id"             => ' nullable | numeric' ,
            "start_hire_date"           => ' required ' ,
            "rank"                      => ' required ' ,
        ];
    }
}
