<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SkillRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_en'=>'required|unique:skills,name_en',
            'name_ar'=>'required|unique:skills,name_ar',
        ];
    }

    public function messages()
    {
        return [
            'name_en.required' => 'Name is required',
            'name_ar.required'  => 'مطلوب اسم',
            'name_en.unique' => 'Name must be unique',
            'name_ar.unique'  => 'يجب أن يكون الاسم فريدًا',
        ];
    }
}
