<?php

namespace App\Listeners;

use App\Events\NewEmployee;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Notifications\NewEmployeeNotification;

class SupervisorNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewEmployee  $event
     * @return void
     */
    public function handle(NewEmployee $event)
    {
        $supervisor = $event->supervisor;
        $employee = $event->employee;

        //notify
        $supervisor->notify(new NewEmployeeNotification($supervisor,$employee));
    }
}
