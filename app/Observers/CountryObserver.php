<?php

namespace App\Observers;

use App\Country;

class CountryObserver
{
    /**
     * Listen to the User deleting event.
     *
     * @param  \App\Country  $country
     * @return void
     */
    public function deleting(Country $country)
    {
        foreach ($country->cities()->get() as $city) {
            $city->delete();
        }
    }
}