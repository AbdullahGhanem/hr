<?php

namespace App\Observers;

use App\Skill;

class SkillObserver
{
    /**
     * Listen to the User deleting event.
     *
     * @param  \App\Skill  $skill
     * @return void
     */
    public function deleting(Skill $skill)
    {
        foreach ($skill->userSkills()->get() as $userSkill) {
            $userSkill->delete();
        }
    }
}