<?php

namespace App\Observers;

use App\Department;

class DepartmentObserver
{
    /**
     * Listen to the User deleting event.
     *
     * @param  \App\Department  $department
     * @return void
     */
    public function deleting(Department $department)
    {
        foreach ($department->jobTitles()->get() as $jobTitle) {
            $jobTitle->delete();
        }
    }
}