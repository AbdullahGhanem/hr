<?php

namespace App\Observers;

use App\User;
use App\Events\NewEmployee;

class UserObserver
{
    /**
     * Listen to the User deleting event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function deleting(User $user)
    {
        foreach ($user->certificates()->get() as $certificate) {
            $certificate->delete();
        }
        foreach ($user->experiences()->get() as $experience) {
            $experience->delete();
        }
        foreach ($user->contracts()->get() as $contract) {
            $contract->delete();
        }
        // foreach ($user->userSkills()->get() as $userSkill) {
        //     // $userSkill->delete();
        //     echo '<pre>' . var_export($user->userSkills, true) . '</pre>';
        //     dd('lol');
           
        // }

        $user->recruitment->delete();
        $user->socialStatus->delete();
    }

    public function created(User $user){
        if ($user->supervisor != null ) {
           //fire event
           event(new NewEmployee($user->supervisor , $user ));
        }
    }
}