<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Recruitment extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'user_id',
        'completed',
        'start_date',
        'end_date',
        'service_availability',
    ];


    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at','deleted_at',
    ];


    /**
     * relation between ( Recruitment::class  -  User::class)
     * (1-1) Relationship  [ user - recruitment ]
     * @return void
     */
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }


    public function scopeRecruitmentExist($query , $user_id)
    {
        return $query->where('user_id',$user_id);
    }

}
