<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewSalary extends Notification implements ShouldQueue
{
    use Queueable;

    public $user;
    public $salary;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user,$salary)
    {
        $this->user = $user;
        $this->salary = $salary;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->markdown(
            'mail.salary',
            [
                'user'=>$this->user,
                'salary'=>$this->salary,
            ]
        );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $data = [
            'body'=>[
                'en'=>[
                    'head'=> trans('notification/newSalary.head',[],'en')  ,
                    'content'=> trans('notification/newSalary.content',[],'en'),
                ],
                'ar'=>[
                    'head'=>trans('notification/newSalary.head',[],'ar'),
                    'content'=>trans('notification/newSalary.content',[],'ar'),
                ],
                'id'=> $this->salary->id,
                'type'=>'NewSalary',
            ]
        ];
        \Log::info($data);
        return $data;
    }
}
