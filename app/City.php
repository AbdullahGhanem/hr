<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class City extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name_en',
        'name_ar',
        'country_id',
    ];


    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at','deleted_at'
    ];

    /**
     * Get name by the specified language
     * @return [String] name
     */
    public function getNameAttribute()
    {
        $name = app('App\Services\MultiLang')->lang == '_ar' ? $this->name_ar : $this->name_en;
        return $name;
    }
    
    /**
     * relation between ( User::class  -  City::class)
     * @return void
     */
    public function users(){
        return $this->hasMany(User::class,'city_id');
    }

    /**
     * relation between ( Country::class  -  City::class)
     * (1-M) Relationship  [ country - cities ]
     * @return void
     */
    public function country(){
        return $this->belongsTo(Country::class,'country_id');
    }

}
