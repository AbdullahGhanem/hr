<?php

namespace App\Providers;


use Illuminate\Support\ServiceProvider;

use App\User;
use App\Department;
use App\JobTitle;
use App\Salary;
use App\Contract;
use App\Country;
use App\Skill;

use App\Observers\UserObserver;
use App\Observers\DepartmentObserver;
use App\Observers\JobTitleObserver;
use App\Observers\SalaryObserver;
use App\Observers\ContractObserver;
use App\Observers\CountryObserver;
use App\Observers\SkillObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);

        Department::observe(DepartmentObserver::class);

        JobTitle::observe(JobTitleObserver::class);

        Salary::observe(SalaryObserver::class);

        Contract::observe(ContractObserver::class);

        Country::observe(CountryObserver::class);

        Skill::observe(SkillObserver::class);


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
