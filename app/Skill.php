<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Skill extends Model
{
    use SoftDeletes ;
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name_en',
        'name_ar',
    ];


    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at','deleted_at',
    ];

    /**
     * Get name by the specified language
     * @return [String] name
     */
    public function getNameAttribute()
    {
        $name = app('App\Services\MultiLang')->lang == '_ar' ? $this->name_ar : $this->name_en;
        return $name;
    }

    /**
     * relation between ( Skill::class  -  UserSkill::class)
     * (1-M) Relationship  [ skill - userSkills ]
     * @return void
     */
    public function userSkills(){
        return $this->hasMany(UserSkill::class,'skill_id');
    }

    public function scopeFilerName($query,$name){
        if ($name) {
            if (\App::getLocale() == 'ar') {
                return  $query->where('name_ar','like',"%{$name}%");
            }else if(\App::getLocale() == 'en'){
                return  $query->where('name_en','like',"%{$name}%");
            }else{
                return  $query->where('name_en','like',"%{$name}%");
            }
        }
    }

}
