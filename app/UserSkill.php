<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class UserSkill extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'user_id',
        'skill_id',
        'skill_level_id',
        'date_acquired',
    ];


    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at','deleted_at'
    ];

    /**
     * relation between ( Skill::class  -  UserSkill::class)
     * (1-M) Relationship  [ skill - userSkills ]
     * @return void
     */
    public function skill(){
        return $this->belongsTo(Skill::class,'skill_id');
    }

    /**
     * relation between ( SkillLevel::class  -  UserSkill::class)
     * (1-M) Relationship  [ skillLevel - userSkills ]
     * @return void
     */
    public function skillLevel(){
        return $this->belongsTo(SkillLevel::class,'skill_level_id');
    }

    /**
     * relation between ( User::class  -  UserSkill::class)
     * (1-M) Relationship  [ user - userSkills ]
     * @return void
     */
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

}
