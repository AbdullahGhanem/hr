<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Authenticatable
{
    use Notifiable ,SoftDeletes ;

    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'name_en',
        'name_ar',
        'address',
        'national_id',
        'job_title_id',
        'email',
        'image',
        'dob',
        'country_id',
        'type_id',
        'gender',
        'mobile_number',
        'supervisor_id',
        'department_id',
        'start_hire_date',
        'rank',
        'total_vacation_days',
        'years_of_experience',
        'social_insurance_Number',
        'default_password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];
    
    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $dates = ['deleted_at'];



    // TYPES
    CONST Rank = [
        'J' =>  'Junior',
        'M' =>  'Middle',
        'S' =>  'Senior',
        'T' =>  'Team-Leader'
    ];

    /**
     * Get name by the specified language
     * @return [String] name
     */
    public function getNameAttribute()
    {
        $name = app('App\Services\MultiLang')->lang == '_ar' ? $this->name_ar : $this->name_en;
        return $name;
    }

    /**
     * Get rank
     * @param [type] $value
     * @return void
     */
    public function getRankAttribute($value)
    {
        return self::Rank[$value] ;
    }







                        /** 
                         * RelationShips
                         * RelationShips
                         * RelationShips
                         * 
                        */

    /**
      * relation between ( User::class  -  UserType::class)
      * @return void
      */
    public function type(){
        return $this->belongsTo(UserType::class);
    }

    /**
      * relation between ( User::class  -  City::class)
      * @return void
      */
    public function country(){
        return $this->belongsTo(Country::class);
    }

    /**
      * relation between ( User::class  -  JobTitle::class)
      * @return void
      */
    public function jobTitle(){
        return $this->belongsTo(JobTitle::class);
    }

    /**
     * relation between ( User::class  -  User::class)
     * (1-M) Relationship  [ supervisor - supervsiorEmployees ]
     * @return void
     */
    public function supervisor()
    {
        return $this->belongsTo(self::class,'supervisor_id');
    }

    /**
     * relation between ( User::class  -  User::class)
     * (1-M) Relationship  [ supervisor - supervsiorEmployees ]
     * @return void
     */
    public function supervsiorEmployees()
    {
        return $this->hasMany(User::class,'supervisor_id');
    }

    /**
     * relation between ( User::class  -  Recruitment::class)
     * (1-1) Relationship  [ user - recruitment ]
     * @return void
     */
    public function recruitment(){
        return $this->hasOne(Recruitment::class);
    }

    /**
     * relation between ( User::class  -  Certificate::class)
     * (1-M) Relationship  [ user - certificate ]
     * @return void
     */
    public function certificates(){
        return $this->hasMany(Certificate::class,'user_id');
    }

    /**
     * relation between ( User::class  -  Experience::class)
     * (1-M) Relationship  [ user - experiences ]
     * @return void
     */
    public function experiences(){
        return $this->hasMany(Experience::class,'user_id');
    }

    /**
     * relation between ( User::class  -  SocialStatus::class)
     * (1-1) Relationship  [ user - socialStatus ]
     * @return void
     */
    public function socialStatus(){
        return $this->hasOne(SocialStatus::class);
    }

    /**
     * relation between ( User::class  -  Contract::class)
     * (1-M) Relationship  [ user - contracts ]
     * @return void
     */
    public function contracts(){
        return $this->hasMany(Contract::class,'user_id');
    }

    /**
     * relation between ( User::class  -  UserSkill::class)
     * (1-M) Relationship  [ user - userSkills ]
     * @return void
     */
    public function userSkills(){
        return $this->hasMany(UserSkill::class,'user_id');
    }


                        /** 
                         * scopes
                         * scopes
                         * scopes
                         * 
                        */

    /**
     * return employees only
     * @param [type] $query
     * @return void
     */
    public function scopeEmployees($query){
        return  $query->whereHas('type', function ($new_query) {
            $new_query->where('id',1);
        });
    }
    /**
     * return admins only
     * @param [type] $query
     * @return void
     */
    public function scopeAdmins($query){
        return  $query->whereHas('type', function ($new_query) {
            $new_query->where('id',2);
        });
    }
    /**
     * return super-admins only
     * @param [type] $query
     * @return void
     */
    public function scopeSuperAdmins($query){
        return  $query->whereHas('type', function ($new_query) {
            $new_query->where('id',3);
        });
    }

    /**
     * return active contracts
     *
     * @param [type] $query
     * @return void
     */
    public function scopeHasActiveContract($query){
        return  $query->whereHas('contracts', function ($new_query) {
            $new_query->contractActive($this->id);
        });
    }

    /**
     * return employees by name like
     *
     * @param [type] $query
     * @return void
     */
    public function scopeFilterName($query,$name){
        if ($name) {
            if (\App::getLocale() == 'ar') {
                return  $query->where('name_ar','like',"%{$name}%");
            }else if(\App::getLocale() == 'en'){
                return  $query->where('name_en','like',"%{$name}%");
            }else{
                return  $query->where('name_en','like',"%{$name}%");
            }
        }
    }
    /**
     * return employees where Superviso name like ...
     *
     * @param [type] $query
     * @return void
     */
    public function scopeFilterSupervisorName($query,$name){
        if ($name) {
            return $query->whereHas('supervisor',function($new_query) use ($name){
                $new_query->filterName($name);
            });
        }
    }

    /**
     * return employees by email like
     *
     * @param [type] $query
     * @return void
     */
    public function scopeFilterEmail($query,$email){
        if ($email) {
            return  $query->where('email','like',"%{$email}%");
        }
    }
    /**
     * return employees by NationalId like
     *
     * @param [type] $query
     * @return void
     */
    public function scopeFilterNationalId($query,$national_id){
        if ($national_id) {
            return  $query->where('national_id','like',"%{$national_id}%");
        }
    }

    /**
     * return employees by Jop Title like
     *
     * @param [type] $query
     * @return void
     */
    public function scopeFilterJopTitle($query,$jopTitle){
        if ($jopTitle) {
            return $query->whereHas('jobTitle',function($q) use ($jopTitle){
                $q->filterName($jopTitle);
            });
        }
    }

    /**
     * return employees by department 
     *
     * @param [type] $query
     * @param [type] $name
     * @return void
     */
    public function scopeFilterDepartment($query , $name){
        if ($name) {  
            return $query->whereHas('jobTitle',function($new_query) use ($name){
                return $new_query->whereHas('department',function($q) use ($name){
                    $q->filterName($name);
                });
            });
        }
    }

    /**
     * scopeSupervisorTeam function
     *
     * @param [type] $query
     * @param [type] $supervisor_id
     * @return void
     */
    public function scopeSupervisorTeam($query , $supervisor_id){
        if ($supervisor_id) {  
            return $query->where('supervisor_id',$supervisor_id);
        }
    }



    /**
     * [type description]
     * return employee type  (  2 -  Is Supervisor  )   (   1 - Is Employee   )  
     * [ employee direct manger ]
     * @return [type] [description]
     */
    public function isSupervisor()
    {
        $employee = self::where('supervisor_id', $this->id);
        return ($employee->first()) ? true : false ;
    }

    /**
     * return supervisors and teams
     * @param [type] $query
     * @return void
     */
    public static function supervisors(){
        $supervisors_ids = self::where('supervisor_id','!=',null)
                            ->groupBy('supervisor_id')
                            ->pluck('supervisor_id');
        $supervisors = self::whereIn('id',$supervisors_ids)->with('supervsiorEmployees');
        return $supervisors;
    }

}
