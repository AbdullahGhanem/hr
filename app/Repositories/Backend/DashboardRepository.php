<?php

namespace App\Repositories\Backend;
use Illuminate\Http\Request;

use Carbon\Carbon;
use App\User;
use App\Department;
use App\Salary;



class DashboardRepository {
   
    /**
     * get_users_count_by_type function
     * count
     * @param [type] $type
     * @return void
     */
    public function get_users_count_by_type($type = null ){
        if ($type) {
            $users = User::where('type_id',$type)->get();
            return $users->count();
        }else{
            $users = User::all();
            return $users->count();
        }
    }

    /**
     * get_count_by_model function
     * model count
     * @param [type] $model
     * @return void
     */
    public function get_count_by_model($model){
        if ($model) {
            return  $model::count();
        }
    }

    /**
     * get_total_salaries_by_column function
     * get sum of column in salary table
     * @param string $column
     * @return void
     */
    public function get_total_salaries_by_column( $column = 'total' ){
        if ($column) {
            $date       = Carbon::now();
            $last_month = $date->subMonth()->format('Y-m-1'); // last-month
            $sum        = Salary::where('date','>', $last_month)
                                ->sum($column);
            return $sum;
        }
    }

    /**
     * get_top_incentive_employees function
     * get top incentive employees 
     * @return void
     */
    public function get_top_incentive_employees(){
        $date       = Carbon::now();
        $last_month = $date->subMonth()->format('Y-m-1'); // last-month
        $salaries   = Salary::with('contract','contract.user','contract.user.jobTitle.department')
                            ->where('date','>', $last_month)
                            ->orderBy('incentive','desc')
                            ->take(4)
                            ->get();
        return $salaries;
    }


}