<?php

namespace App\Repositories\Backend;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Salary;

class SalaryRepository {

   public function calculateSalary($request){
        $monthly_salary = ($request->get('monthly_salary'))? $request->get('monthly_salary') : 0.0;
        $allowance = ($request->get('allowance'))? $request->get('allowance') : 0.0;
        $incentive = ($request->get('incentive'))? $request->get('incentive') : 0.0;
        $other = ($request->get('other'))? $request->get('other') : 0.0;
        $deduction = ($request->get('deduction'))? $request->get('deduction') : 0.0;
        $loan = ($request->get('loan'))? $request->get('loan') : 0.0;
        $tax = ($request->get('tax'))? $request->get('tax') : 0.0;

        $net_salary = $monthly_salary + ($allowance + $incentive + $other);
        $net_salary =  $net_salary - ($deduction);
        $total = $net_salary - ($loan + $tax );

        return [
            'net_salary'  => $net_salary,
            'total'  => $total,
        ];
   }
}